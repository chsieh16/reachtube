from typing import Any, Dict, List

# TODO Avoid accessing protected module
from reachtube.set_impl._visitor_base import SetImplVisitorBase, \
    EmptySet, SetIntersection, SetUnion, SetDifference, CustomSetImplBase
from reachtube.rect_set_impl import BoxesMap
from reachtube.contiguous_union_set_impl import ContiguousUnion

_ResultT = Dict[str, List[Any]]


class ToYamlType(SetImplVisitorBase[_ResultT]):
    def visit_empty_set(self, tree: EmptySet) -> _ResultT:
        return {}

    def visit_intersection(self, tree: SetIntersection) -> _ResultT:
        return {"Intersection": [self.visit(op) for op in tree.operands]}

    def visit_union(self, tree: SetUnion) -> _ResultT:
        return {"Union": [self.visit(op) for op in tree.operands]}

    def visit_difference(self, tree: SetDifference) -> _ResultT:
        return {"Difference": [self.visit(op) for op in tree.operands]}

    def visit_custom_set(self, tree: CustomSetImplBase) -> _ResultT:
        if isinstance(tree, BoxesMap):
            return {"BoxesMap": [[box.mins.tolist(), box.maxes.tolist()]for box in tree.boxes]}
        if isinstance(tree, ContiguousUnion):
            return {"ContiguousUnion": [[t, self.visit(region)] for t, region in tree]}
        else:
            raise NotImplementedError("No conversion from %s to Yaml list or dictionary" % tree.__class__.__name__)

# TODO JSON, base64, etc.
