from bisect import bisect_right
from itertools import dropwhile
from typing import Iterator, Sequence, Tuple, TypeVar

from reachtube._abstract_types import SupportPointStamped
from reachtube.set_impl import CustomSetImplBase, EmptinessQuery, MembershipQuery, SetImplBase, EmptySet

_StampT = TypeVar('_StampT')
_PointT = TypeVar('_PointT')


class ContiguousUnion(CustomSetImplBase):
    """
    ContiguousUnion has a special axis (usually the time axis) that are
    contiguous intervals.
    As a result, the sets in the union are disjoint.

    Notes:
        * Stamp interval is interpreted as right-open, i.e., [t_1, t_2).
        * For stamp intervals not covered by the contract, it means a device
          can be at any point, i.e., [-inf, inf] at those stamps.
    """

    def __init__(self, sorted_stamp_seq: Sequence[_StampT], set_impl_seq: Sequence[SetImplBase]):
        assert len(sorted_stamp_seq) == len(set_impl_seq)
        assert len(sorted_stamp_seq) >= 1
        # NOTE: Stamps must be strictly increasing
        assert all(sorted_stamp_seq[i] < sorted_stamp_seq[i+1]
                   for i in range(len(sorted_stamp_seq) - 1))
        self._stamp_seq = list(sorted_stamp_seq)
        self._set_impl_seq = list(set_impl_seq)

    def __len__(self) -> int:
        assert len(self._stamp_seq) == len(self._set_impl_seq)
        return len(self._stamp_seq)

    def __iter__(self) -> Iterator[Tuple[_StampT, SetImplBase]]:
        assert len(self._stamp_seq) == len(self._set_impl_seq)
        return zip(self._stamp_seq, self._set_impl_seq)

    def contains(self, x: SupportPointStamped[_StampT, _PointT]) -> bool:
        idx = bisect_right(self._stamp_seq, x.stamp) - 1  # Binary search using stamp
        return MembershipQuery(x.position).visit(self._set_impl_seq[idx])

    def isempty(self) -> bool:
        return all(EmptinessQuery().visit(impl) for impl in self._set_impl_seq)

    def simplify(self) -> None:
        self._set_impl_seq = list(dropwhile(lambda x: isinstance(x, EmptySet),
                                            self._set_impl_seq))

        self._stamp_seq = self._stamp_seq[-len(self._set_impl_seq):] if len(self._set_impl_seq) > 0 else []
        assert len(self._stamp_seq) == len(self._set_impl_seq)
