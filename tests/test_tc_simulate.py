import timeit
import numpy as np

import reachtube

if __name__ == "__main__":
    start_time = timeit.default_timer()

    reachtube.tc_simulate('follow_waypoint', [2.0, 2.0, 5.0],
                           0.03125, 200,
                           np.zeros(9))

    elapsed = timeit.default_timer() - start_time
    print("Duration:", elapsed)
