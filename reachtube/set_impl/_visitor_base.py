import abc

from typing import Any, Generic, Sequence, TypeVar
from typing_extensions import final

_ResultT = TypeVar('_ResultT')


class SetImplVisitorBase(Generic[_ResultT], abc.ABC):
    @final
    def visit(self, tree: 'SetImplBase') -> _ResultT:
        return tree.accept(self)

    @abc.abstractmethod
    def visit_empty_set(self, tree: 'EmptySet') -> _ResultT:
        raise NotImplementedError

    @abc.abstractmethod
    def visit_intersection(self, tree: 'SetIntersection') -> _ResultT:
        raise NotImplementedError

    @abc.abstractmethod
    def visit_union(self, tree: 'SetUnion') -> _ResultT:
        raise NotImplementedError

    @abc.abstractmethod
    def visit_difference(self, tree: 'SetDifference') -> _ResultT:
        raise NotImplementedError

    @abc.abstractmethod
    def visit_custom_set(self, tree: 'CustomSetImplBase') -> _ResultT:
        raise NotImplementedError


class SetImplBase(abc.ABC):
    @abc.abstractmethod
    def accept(self, visitor: SetImplVisitorBase[_ResultT]) -> _ResultT:
        raise NotImplementedError


@final
class EmptySet(SetImplBase):
    """ Empty set """
    def accept(self, visitor: SetImplVisitorBase[_ResultT]) -> _ResultT:
        return visitor.visit_empty_set(self)

    def __eq__(self, other: Any) -> bool:
        return isinstance(other, self.__class__)


class CompositeSetImplBase(SetImplBase, abc.ABC):
    def __init__(self, operands: Sequence[SetImplBase]):
        if not operands or len(operands) < 2:
            raise ValueError(self.__class__.__name__ + " of less than two sets is not allowed.")
        self._operands = tuple(operands)  # type: Sequence[SetImplBase]

    @property
    def operands(self) -> Sequence[SetImplBase]:
        assert len(self._operands) >= 2
        return self._operands


@final
class SetIntersection(CompositeSetImplBase):
    def accept(self, visitor: SetImplVisitorBase[_ResultT]) -> _ResultT:
        return visitor.visit_intersection(self)


@final
class SetUnion(CompositeSetImplBase):
    def accept(self, visitor: SetImplVisitorBase[_ResultT]) -> _ResultT:
        return visitor.visit_union(self)


@final
class SetDifference(CompositeSetImplBase):
    def accept(self, visitor: SetImplVisitorBase[_ResultT]) -> _ResultT:
        return visitor.visit_difference(self)


class CustomSetImplBase(SetImplBase, abc.ABC):
    @final
    def accept(self, visitor: SetImplVisitorBase[_ResultT]) -> _ResultT:
        return visitor.visit_custom_set(self)

    def __contains__(self, item: Any) -> bool:
        return self.contains(item)

    def __bool__(self) -> bool:
        return not self.isempty()

    @abc.abstractmethod
    def contains(self, x: Any) -> bool:
        raise NotImplementedError

    @abc.abstractmethod
    def isempty(self) -> bool:
        raise NotImplementedError


def set_impl_union(*impls: SetImplBase) -> SetImplBase:
    if len(impls) > 1:
        return SetUnion(impls)
    elif len(impls) == 1:
        return impls[0]
    else:
        return EmptySet()


def set_impl_intersection(*impls: SetImplBase) -> SetImplBase:
    if len(impls) > 1:
        return SetIntersection(impls)
    elif len(impls) == 1:
        return impls[0]
    # else:
    raise ValueError("Intersection of empty collection of sets is undefined"
                     " because universal set is not necessarily defined.")


def set_impl_difference(*impls: SetImplBase) -> SetImplBase:
    if len(impls) > 1:
        return SetDifference(impls)
    elif len(impls) == 1:
        return impls[0]
    # else:
    raise ValueError("Difference of empty collection of sets is undefined.")
