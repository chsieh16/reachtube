from typing import Any, List, Sequence, Tuple, Generator

from reachtube.contiguous_union_set_impl._contiguous_union import ContiguousUnion
from reachtube.rect_set_impl import SimplifyBoxes
from reachtube.set_impl import SimplifierBase, SetImplBase, \
    CustomSetImplBase, EmptySet, \
    set_impl_difference, set_impl_intersection, set_impl_union, \
    EmptySetSimplifier, Flatten


class SimplifyContiguousUnion(SimplifierBase):

    def simplify_intersection_operands(self, operands: Sequence[SetImplBase]) \
            -> Sequence[SetImplBase]:
        # FIXME Currently only support set intersection of two sets
        if len(operands) != 2 or not all(isinstance(op, ContiguousUnion) for op in operands):
            return operands  # Unable to simplify
        return [_simplify_binary_operator(set_impl_intersection, *operands[0:2])]

    def simplify_union_operands(self, operands: Sequence[SetImplBase]) \
            -> Sequence[SetImplBase]:
        if sum(isinstance(op, ContiguousUnion)
               for op in operands) < 2:
            return operands  # Unable to simplify
        # FIXME Currently only support set union of two sets
        if len(operands) != 2 or not all(isinstance(op, ContiguousUnion) for op in operands):
            return operands  # Unable to simplify
        return [_simplify_binary_operator(set_impl_union, *operands[0:2])]

    def simplify_difference_operands(self, operands: Sequence[SetImplBase]) \
            -> Sequence[SetImplBase]:
        # FIXME Currently only support set difference of two sets
        if len(operands) != 2 or not all(isinstance(op, ContiguousUnion) for op in operands):
            return operands  # Unable to simplify
        return [_simplify_binary_operator(set_impl_difference, *operands[0:2])]

    def simplify_custom_set(self, impl: CustomSetImplBase) \
            -> SetImplBase:
        if not isinstance(impl, ContiguousUnion):
            return impl
        # TODO simplify when the regions are the same in consecutive time stamps
        impl.simplify()
        return impl


def _simplify_binary_operator(op_func, a: ContiguousUnion, b: ContiguousUnion) \
        -> ContiguousUnion:
    t_list = []
    set_impl_list = []
    for t, set_a, set_b in _gen_stamp_aligned_pairs(a, b):
        impl = op_func(set_a, set_b)
        # TODO apply more simplifications?
        impl = SimplifyBoxes().visit(impl)
        impl = Flatten().visit(impl)
        impl = EmptySetSimplifier().visit(impl)
        t_list.append(t)
        set_impl_list.append(impl)
    return ContiguousUnion(t_list, set_impl_list)


def _gen_stamp_aligned_pairs(a: ContiguousUnion,
                             b: ContiguousUnion) \
        -> Generator[Tuple[Any, SetImplBase, SetImplBase],
                     None, None]:
    it_a, it_b = iter(a), iter(b)
    stamped_set_a, stamped_set_b = next(it_a, None), next(it_b, None)
    prev_set_a = prev_set_b = EmptySet()  # type: SetImplBase

    # NOTE the loop is bounded by the sum of the length because
    # the worst case is when they do not share any stamp
    for _ in range(len(a) + len(b)):
        if stamped_set_a is None or stamped_set_b is None:
            break  # One of them is exhausted

        t_a, set_a = stamped_set_a
        t_b, set_b = stamped_set_b
        if t_a < t_b:
            yield t_a, set_a, prev_set_b
            stamped_set_a = next(it_a, None)
            prev_set_a = set_a
        elif t_a > t_b:
            yield t_b, prev_set_a, set_b
            stamped_set_b = next(it_b, None)
            prev_set_b = set_b
        else:  # t_a == t_b
            yield t_a, set_a, set_b
            stamped_set_a, stamped_set_b = next(it_a, None), next(it_b, None)
            prev_set_a, prev_set_b = set_a, set_b

    if stamped_set_a is not None:
        t_a, set_a = stamped_set_a
        yield t_a, set_a, prev_set_b
        yield from ((t_a, set_a, prev_set_b) for t_a, set_a in it_a)
    elif stamped_set_b is not None:
        t_b, set_b = stamped_set_b
        yield t_b, prev_set_a, set_b
        yield from ((t_b, prev_set_a, set_b) for t_b, set_b in it_b)
