"""
Collection of abstract classes representing mathematical objects.
Not exposed to users.
"""

import abc
from typing import Any, Generic, Sequence, TypeVar
from typing_extensions import Protocol

_ElemT = TypeVar('_ElemT')


class NaiveSetMixin(Generic[_ElemT]):
    """
    Abstract class representing a set of elements in arbitrary dimensions.
    """

    def __contains__(self, x: _ElemT) -> bool:
        return self.contains(x)

    def __bool__(self) -> bool:
        return not self.isempty()

    def __le__(self, other: Any) -> bool:
        return self.issubset(other)

    def __lt__(self, other: Any) -> bool:
        return self.ispropersubset(other)

    def __ge__(self, other: Any) -> bool:
        return self.issuperset(other)

    def __gt__(self, other: Any) -> bool:
        return self.ispropersuperset(other)

    def __and__(self, other: Any) -> 'NaiveSetMixin[_ElemT]':
        return self.intersection(other)

    @abc.abstractmethod
    def contains(self, x: _ElemT) -> bool:
        """ Check if ``x ∈ self``
        Raises
        ------
        NotImplementedError
            If this cannot be exactly determined with current implementation.
        """
        raise NotImplementedError

    @abc.abstractmethod
    def isempty(self) -> bool:
        """ Check if ``self == ∅``
        Raises
        ------
        NotImplementedError
            If this cannot be exactly determined with current implementation.
        """
        raise NotImplementedError

    @abc.abstractmethod
    def isdisjoint(self, other: Any) -> bool:
        """ Check if ``self`` is disjoint with ``other``
        Raises
        ------
        NotImplementedError
            If this cannot be exactly determined with current implementation.
        """
        raise NotImplementedError

    @abc.abstractmethod
    def issubset(self, other: Any) -> bool:
        """ Check if ``self`` is a subset of ``other``
        Raises
        ------
        NotImplementedError
            If this cannot be exactly determined with current implementation.
        """
        raise NotImplementedError

    def ispropersubset(self, other: Any) -> bool:
        """ Check if ``self`` is a proper subset of ``other``
        Raises
        ------
        NotImplementedError
            If this cannot be exactly determined with current implementation.
        """
        return self.issubset(other) and not self.issuperset(other)

    def issuperset(self, other: Any) -> bool:
        """ Check if ``self`` is a superset of ``other``
        Raises
        ------
        NotImplementedError
            If this cannot be exactly determined with current implementation.
        """
        assert isinstance(other, NaiveSetMixin)
        return other.issubset(self)

    def ispropersuperset(self, other: Any) -> bool:
        """ Check if ``self`` is a superset of ``other``
        Raises
        ------
        NotImplementedError
            If this cannot be exactly determined with current implementation.
        """
        return self.issuperset(other) and not self.issubset(other)

    @abc.abstractmethod
    def intersection(self, *others: Any) -> 'NaiveSetMixin[_ElemT]':
        """ Return set intersection
        Raises
        ------
        NotImplementedError
            If this cannot be exactly represented with current implementation.
        """
        raise NotImplementedError


_StampT = TypeVar('_StampT', covariant=True)
_PointT = TypeVar('_PointT', covariant=True)
_StateT = TypeVar('_StateT', covariant=True)
_ArrayLike = Sequence[Any]  # TODO Restrict to array like types


class SupportPointStamped(Protocol[_StampT, _PointT]):
    """
    Interface to access time stamp and position
    """
    @property
    @abc.abstractmethod
    def stamp(self) -> _StampT:
        """ Projection to stamp domain """
        raise NotImplementedError

    @property
    @abc.abstractmethod
    def position(self) -> _PointT:
        """ Projection to point domain """
        raise NotImplementedError

    @abc.abstractmethod
    def to_array_like(self) -> _ArrayLike:
        """ Convert to array like data type for NumPy """
        raise NotImplementedError


class SupportStateStamped(SupportPointStamped[_StampT, _PointT],
                          Protocol[_StampT, _PointT, _StateT]):
    """
    Interface to access time stamp, position, and state.
    Note:
        + state may or may not consist of position.
          How to derive position from state can be customized.
        + Also have to be `array_like`
    """
    @property
    @abc.abstractmethod
    def state(self) -> _StateT:
        """ Projection to state domain """
        raise NotImplementedError
