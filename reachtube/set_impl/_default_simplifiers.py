from typing import Sequence

from reachtube.set_impl._simplifier_base import SimplifierBase
from reachtube.set_impl._visitor_base import SetImplBase, EmptySet, SetIntersection, SetUnion, SetDifference


class Flatten(SimplifierBase):
    """
    Flatten composite set implementations

    Notes:
        Implementing following rules

        * ⋂ [ ⋂ SS, ...] = ⋂ (SS + [...])
        * ⋃ [ ⋃ SS, ...] = ⋃ (SS + [...])
        * S / [⋃ SS, ...] = S / (SS + [...])
        * (S / SS) / [...] = S / (SS + [...])
    """

    def simplify_intersection_operands(self, operands: Sequence[SetImplBase]) \
            -> Sequence[SetImplBase]:
        if not any(isinstance(op, SetIntersection) for op in operands):
            return operands

        flatten_ops = []
        for op in operands:
            if not isinstance(op, SetIntersection):
                flatten_ops.append(op)
            else:
                flatten_ops.extend(op.operands)
        return flatten_ops

    def simplify_union_operands(self, operands: Sequence[SetImplBase]) \
            -> Sequence[SetImplBase]:
        if not any(isinstance(op, SetUnion) for op in operands):
            return operands

        flatten_ops = []
        for op in operands:
            if not isinstance(op, SetUnion):
                flatten_ops.append(op)
            else:
                flatten_ops.extend(op.operands)
        return flatten_ops

    def simplify_difference_operands(self, operands: Sequence[SetImplBase]) \
            -> Sequence[SetImplBase]:
        head, tail = operands[0], operands[1:]
        if isinstance(head, SetDifference):
            tail = list(head.operands[1:]) + list(operands[1:])
            head = head.operands[0]

        return [head] + list(self.simplify_union_operands(tail))


class EmptySetSimplifier(SimplifierBase):
    """
    Default simplification for operations over empty sets.

    Notes:
        Implementing following rules

        * ⋂ [∅, ...] = ∅
        * ⋃ [∅, ...] = ⋃ [...]
        * ∅ / [S, ...] = ∅
        * S / [∅, ...] = S / [...]
        * S / [S, ...] = ∅
    """

    def simplify_intersection_operands(self, operands: Sequence[SetImplBase]) \
            -> Sequence[SetImplBase]:
        for op in operands:
            if isinstance(op, EmptySet):
                return [op]
        return operands

    def simplify_union_operands(self, operands: Sequence[SetImplBase]) \
            -> Sequence[SetImplBase]:
        return [op for op in operands if not isinstance(op, EmptySet)]

    def simplify_difference_operands(self, operands: Sequence[SetImplBase]) \
            -> Sequence[SetImplBase]:
        assert len(operands) >= 2
        head, tail = operands[0], operands[1:]
        if isinstance(head, EmptySet):
            return [head]  # Difference is empty set

        nonempty_tail = [op for op in tail if not isinstance(op, EmptySet)]
        if any(head == op for op in nonempty_tail):
            # S / S = ∅
            return [EmptySet()]
        else:
            return [head] + nonempty_tail
