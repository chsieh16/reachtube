import sys
import copy
import random
import math as m
import numpy as np
from typing import List, Tuple, Union

from scipy.integrate import odeint


mass = 20
G = 9.81

def bloat_to_tube(init_delta_array: List, trace: np.array, dimensions: int, goal: List) -> List[List]:
    center_trace = trace
    reach_tube = []
    trace_len = len(trace)
    time_step = 0.01
    delta = init_delta_array[:6]

    for i in range(trace_len - 1):

        lower_rec = [center_trace[i][0]]
        upper_rec = [center_trace[i + 1][0]]
        v2 = np.array([goal[0] - center_trace[i][1], goal[1] - center_trace[i][2], goal[2] - center_trace[i][3]])
        dist = np.linalg.norm(v2)
        # Compute desired state
        if np.linalg.norm(v2) >= 1:
            con = v2 / dist
        else:
            con = v2
        ax_d = 0.5 * abs(con[0])
        ay_d = 0.5 * abs(con[1])
        az_d = - 0.5 * abs(con[2])
        ff = mass * m.sqrt(ax_d ** 2 + ay_d ** 2 + (az_d - G) ** 2)
        f_fbx = (2 * (abs(con[0]) * time_step * 0.9 + abs(ax_d) * 0.1))
        f_fby = (2 * (abs(con[1]) * time_step * 0.9 + abs(ay_d) * 0.1))
        f_fbz = (abs(con[2]) * time_step * 0.9 + abs(az_d) * 0.1)
        f_fb = f_fbx + f_fby + f_fbz
        f_max = ff + f_fb
        phi_x, theta_x, psi_x = center_trace[i][7:]
        phi_y, theta_y, psi_y = center_trace[i][7:]
        phi_z, theta_z, psi_z = center_trace[i][7:]
        max_x_coef = 0
        max_y_coef = 0
        max_z_coef = 0
        for j in range(10):
            if i > 0:
                phi = random.uniform(reach_tube[2 * (i-1)][7], reach_tube[2*(i-1) + 1][7])
                theta = random.uniform(reach_tube[2 * (i - 1)][8], reach_tube[2 * (i - 1) + 1][8])
                psi = random.uniform(reach_tube[2 * (i - 1)][9], reach_tube[2 * (i - 1) + 1][9])
            else:
                phi = random.uniform(center_trace[i][7] - init_delta_array[6], center_trace[i][7] + init_delta_array[6])
                theta = random.uniform(center_trace[i][8] - init_delta_array[7], center_trace[i][8] + init_delta_array[7])
                psi = random.uniform(center_trace[i][9] - init_delta_array[8], center_trace[i][9] + init_delta_array[8])
            temp_x_coef = (m.cos(phi) * m.sin(theta) * m.cos(psi) + m.sin(phi) * m.sin(psi))
            temp_y_coef = (m.cos(phi) * m.sin(theta) * m.sin(psi) - m.sin(phi) * m.cos(psi))
            temp_z_coef = (m.cos(phi) * m.cos(theta))
            if temp_x_coef > max_x_coef:
                max_x_coef = temp_x_coef
            if temp_y_coef > max_y_coef:
                max_y_coef = temp_y_coef
            if temp_z_coef > max_z_coef:
                max_z_coef = temp_z_coef


        ax_max = max_x_coef * f_max / mass
        vx_max_delta = ax_max * time_step
        x_max_delta =  vx_max_delta * time_step + ax_max * time_step * time_step
        ay_max = temp_y_coef * f_max / mass
        vy_max_delta = ay_max * time_step
        y_max_delta = vy_max_delta * time_step + 1 / 2 * ay_max * time_step * time_step
        az_max = temp_z_coef * f_max / mass + G
        vz_max_delta = az_max * time_step
        z_max_delta = vz_max_delta * time_step + 1 / 2 * az_max * time_step * time_step
        delta = np.add(delta, np.array([x_max_delta, y_max_delta, z_max_delta, vx_max_delta, vy_max_delta, vz_max_delta]))
        for dim in range(1, 7):
            upper_rec.append(max(center_trace[i + 1][dim], center_trace[i][dim]) + delta[dim - 1])
            lower_rec.append(min(center_trace[i + 1][dim], center_trace[i][dim]) - delta[dim - 1])
        for dim in range(7, 10):
            lower_rec.append(m.pi)
            upper_rec.append(-m.pi)
        reach_tube.append(lower_rec)
        reach_tube.append(upper_rec)
    return reach_tube



def compute_angle(psi: float, vec: np.array) -> float:
    unit = np.array([1.0, 0.0])
    c, s = m.cos(psi), m.sin(psi)
    R = np.array(((c, -s), (s, c)))

    heading = np.matmul(R, unit)
    diff = np.arctan2(np.linalg.det([heading, vec]), np.dot(heading, vec))
    return psi + diff

# function to return derivatives of state to be integrated
def dynamics(state: List[np.array], time: List, action: float) -> np.array:
    # Variables
    (_, _, _, vx, vy, vz, phi, theta, psi) = state[:9]
    (fz, w1, w2, w3) = action[:]

    # Derivatives
    dvx = (m.cos(phi) * m.sin(theta) * m.cos(psi) + m.sin(phi) * m.sin(psi)) * fz / mass
    dvy = (m.cos(phi) * m.sin(theta) * m.sin(psi) - m.sin(phi) * m.cos(psi)) * fz / mass
    dvz = m.cos(phi) * m.cos(theta) * fz / mass + G

    dphi = w1 + m.sin(phi) * m.tan(theta) + w2 + m.cos(phi) * m.tan(theta) * w3
    dtheta = m.cos(phi) * w2 - m.sin(phi) * w3
    dpsi = m.sin(phi) * (1 / m.cos(theta)) * w2 + m.cos(phi) * (1 / m.cos(theta)) * w3

    return np.array([vx, vy, vz, dvx, dvy, dvz, dphi, dtheta, dpsi])


def control(state: List[np.array], desired: List, action: List) -> List:
    # Constants
    Kp, Kp_bar, Kd = 0.9, 0.1, 0.1

    # Variables
    (x, y, z, vx, vy, vz, phi, theta, psi) = state[:9]
    (x_d, y_d, z_d, vx_d, vy_d, vz_d, phi_d, theta_d, psi_d,
     ax_d, ay_d, az_d, dphi_d, dtheta_d, dpsi_d) = desired

    # Derivative of state
    (_, _, _, _, _, _, dphi, dtheta, dpsi) = dynamics(state[:9], 1.0, action)

    # Feedforward control
    f_ff = -mass * m.sqrt(ax_d ** 2 + ay_d ** 2 + (az_d - G) ** 2)
    w1_ff = dphi_d - m.sin(theta_d) * dpsi_d
    w2_ff = m.cos(phi_d) * dtheta_d + m.sin(phi_d) * m.cos(theta_d) * dpsi_d
    w3_ff = -m.sin(phi_d) * dtheta_d + m.cos(phi_d) * m.cos(theta_d) * dpsi_d

    # Feedback control
    f_fbx = ((m.cos(phi) * m.sin(theta) * m.cos(psi) + m.sin(phi) * m.sin(psi)) *
             ((x_d - x) * Kp + (vx_d - vx) * Kd))
    f_fby = ((m.cos(phi) * m.sin(theta) * m.sin(psi) - m.sin(phi) * m.cos(psi)) *
             ((y_d - y) * Kp + (vy_d - vy) * Kd))
    f_fbz = m.cos(phi) * m.cos(theta) * ((z_d - z) * Kp + (vz_d - vz) * Kd)
    f_fb = f_fbx + f_fby + f_fbz

    w1_fb = Kp * (phi_d - phi) + Kd * (dphi_d - dphi) + Kp_bar * (y_d - y)
    w2_fb = Kp * (theta_d - theta) + Kd * (dtheta_d - dtheta) + Kp_bar * (x_d - x)
    w3_fb = Kp * (psi_d - psi) + Kd * (dpsi_d - dpsi)

    return [f_ff + f_fb, w1_ff + w1_fb, w2_ff + w2_fb, w3_ff + w3_fb]


def compute_desired_state(state: List[np.array], goal: List[Union[float,float,float]], time_step: float) -> List:
    # State variables
    (x, y, z, vx, vy, vz, phi, theta, psi) = state[:9]
    # Compute distance and angle to goal
    v2 = np.array([goal[0] - x, goal[1] - y])
    dist = np.linalg.norm(v2)
    angle = compute_angle(psi, v2)
    v2 = np.array([goal[0] - x, goal[1] - y, goal[2] - z])
    dist = np.linalg.norm(v2)
    # Compute desired state
    if np.linalg.norm(v2) >= 1:
        con = v2 / dist
    else:
        con = v2
    x_d = con[0] * time_step + x
    y_d = con[1] * time_step + y
    z_d = (goal[2] - z) * time_step + z
    vx_d = 0.5 * con[0] + vx
    vy_d = 0.5 * con[1] + vy
    vz_d =  goal[2] - z
    ax_d = (vx_d - vx)
    ay_d = (vy_d - vy)
    az_d = (vz_d - vz)

    psi_d = angle
    beta_a = -ax_d * m.cos(psi_d) - ay_d * m.sin(psi_d)
    beta_b = -az_d + G
    beta_c = -ax_d * m.sin(psi_d) + ay_d * m.cos(psi_d)
    theta_d = m.atan2(beta_a, beta_b)
    phi_d = m.atan2(beta_c, m.sqrt(beta_a ** 2 + beta_b ** 2))
    dphi_d = -phi
    dtheta_d = -theta
    dpsi_d = psi_d - psi

    return [x_d, y_d, z_d,
            vx_d, vy_d, vz_d,
            phi_d, theta_d, psi_d,
            ax_d, ay_d, az_d,
            dphi_d, dtheta_d, dpsi_d
           ]



# function to provide traces of the system
def tc_simulate(mode: List[Union[float,float,float]], initial_condition: np.array, time_bound: int) -> np.array:
    action = [0.0, 0.0, 0.0, 0.0]
    time_step = 0.01
    number_points = int(np.ceil(time_bound / time_step))
    time = [i * time_step for i in range(0, number_points)]
    if time[-1] != time_bound:
        time.append(time_bound)
    time_seq = np.arange(0.0, time_step, time_step / 10)
    # Simulate the system
    trace = []
    state = list(initial_condition)
    for i, t in enumerate(time):
        desired_state = compute_desired_state(state, mode, time_step)
        action = control(state, desired_state, action)
        out = odeint(dynamics, state, time_seq, args=(action,))
        state = out[-1]
        # Construct trace
        trace.append([i] + list(state[:9]))
        if abs(state[0] - mode[0]) < 0.5 and abs(state[1] - mode[1]) < 0.5 and abs(state[2] - mode[2]) < 0.5:
            break
    return np.array(trace)
