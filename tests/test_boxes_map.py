import unittest

from scipy.spatial import Rectangle

from reachtube.rect_set_impl import BoxesMap, SimplifyBoxes
from reachtube.set_impl import set_impl_difference, EmptinessQuery


class MyTestCase(unittest.TestCase):

    def test_set_difference(self):
        rect_1 = Rectangle(mins=[5.8, -0.2, 0.8], maxes=[6.2, 0.2, 1.2])
        rect_2 = Rectangle(mins=[-0.2, -0.2, -0.2], maxes=[0.2, 0.2, 1.2])

        a = BoxesMap.from_rectangles([rect_1, rect_2])
        b = BoxesMap.from_rectangles([rect_1, rect_2])

        diff_impl = set_impl_difference(a, b)
        diff_impl = SimplifyBoxes().visit(diff_impl)
        self.assertTrue(EmptinessQuery().visit(diff_impl))


if __name__ == '__main__':
    unittest.main()
