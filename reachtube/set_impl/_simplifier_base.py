import abc
from typing import Sequence

from reachtube.set_impl._visitor_base import SetImplVisitorBase, \
    SetImplBase, EmptySet, SetIntersection, SetUnion, SetDifference, CustomSetImplBase, \
    set_impl_intersection, set_impl_union, set_impl_difference


class SimplifierBase(SetImplVisitorBase[SetImplBase], abc.ABC):
    """
    Simplifier that applies simplification on each node in post-order.
    Notes:
        Default implementation is to visit each node without modifying anything.
        The subtrees of the given tree should remain UNMODIFIED!!!
    """

    def visit_empty_set(self, tree: EmptySet) -> SetImplBase:
        return tree

    def visit_intersection(self, tree: SetIntersection) -> SetImplBase:
        visited_ops = tuple(self.visit(op) for op in tree.operands)
        simplified_ops = self.simplify_intersection_operands(visited_ops)
        if same_operands(simplified_ops, tree.operands):
            return tree  # Nothing is simplified
        else:
            return set_impl_intersection(*simplified_ops)

    def visit_union(self, tree: SetUnion) -> SetImplBase:
        visited_ops = tuple(self.visit(op) for op in tree.operands)
        simplified_ops = self.simplify_union_operands(visited_ops)
        if same_operands(simplified_ops, tree.operands):
            return tree  # Nothing is simplified
        else:
            return set_impl_union(*simplified_ops)

    def visit_difference(self, tree: SetDifference) -> SetImplBase:
        visited_ops = tuple(self.visit(op) for op in tree.operands)
        simplified_ops = self.simplify_difference_operands(visited_ops)
        if same_operands(simplified_ops, tree.operands):
            return tree  # Nothing is simplified
        else:
            return set_impl_difference(*simplified_ops)

    def visit_custom_set(self, tree: CustomSetImplBase) -> SetImplBase:
        return self.simplify_custom_set(tree)

    def simplify_intersection_operands(self, operands: Sequence[SetImplBase]) \
            -> Sequence[SetImplBase]:
        return operands

    def simplify_union_operands(self, operands: Sequence[SetImplBase]) \
            -> Sequence[SetImplBase]:
        return operands

    def simplify_difference_operands(self, operands: Sequence[SetImplBase]) \
            -> Sequence[SetImplBase]:
        return operands

    def simplify_custom_set(self, impl: CustomSetImplBase) -> SetImplBase:
        return impl


def same_operands(a: Sequence[SetImplBase], b: Sequence[SetImplBase]) -> bool:
    if len(a) != len(b):
        return False
    if a == b:
        return True
    return all(a_op == b_op for a_op, b_op in zip(a, b))


class BoundingBase(SetImplVisitorBase[SetImplBase], abc.ABC):
    """
    Class that compute upper or lower bounding sets on each node in post-order.
    Both upper and lower bounds are required for handling set difference.
    Notes:
        Default implementation is to visit each node without modifying anything.
        The subtrees of the given tree should remain UNMODIFIED!!!
    """
    def __init__(self, upper: bool):
        super(BoundingBase, self).__init__()
        self.__upper = upper  # Not exposed to child classes

    def visit_empty_set(self, tree: EmptySet) -> SetImplBase:
        return tree

    def visit_intersection(self, tree: SetIntersection) -> SetImplBase:
        visited_ops = tuple(self.visit(op) for op in tree.operands)
        if self.__upper:
            approx_ops = self.upper_bound_intersection_operands(visited_ops)
        else:
            approx_ops = self.lower_bound_intersection_operands(visited_ops)
        if same_operands(approx_ops, tree.operands):
            return tree  # Nothing is approximated
        else:
            return set_impl_intersection(*approx_ops)

    def visit_union(self, tree: SetUnion) -> SetImplBase:
        visited_ops = tuple(self.visit(op) for op in tree.operands)
        if self.__upper:
            approx_ops = self.upper_bound_union_operands(visited_ops)
        else:
            approx_ops = self.lower_bound_union_operands(visited_ops)
        if same_operands(approx_ops, tree.operands):
            return tree  # Nothing is approximated
        else:
            return set_impl_union(*approx_ops)

    def visit_difference(self, tree: SetDifference) -> SetImplBase:
        assert len(tree.operands) >= 1
        visited_ops = [self.visit(tree.operands[0])]

        self.__upper = not self.__upper  # Negate the flag for complement
        visited_ops.extend(self.visit(op) for op in tree.operands[1:])
        self.__upper = not self.__upper  # Restore the flag

        if self.__upper:
            approx_ops = self.upper_bound_difference_operands(visited_ops)
        else:
            approx_ops = self.lower_bound_difference_operands(visited_ops)
        if same_operands(approx_ops, tree.operands):
            return tree  # Nothing is approximated
        else:
            return set_impl_difference(*approx_ops)

    def visit_custom_set(self, tree: CustomSetImplBase) -> SetImplBase:
        if self.__upper:
            return self.upper_bound_custom_set(tree)
        else:
            return self.lower_bound_custom_set(tree)

    def upper_bound_intersection_operands(self, operands: Sequence[SetImplBase]) \
            -> Sequence[SetImplBase]:
        return operands

    def upper_bound_union_operands(self, operands: Sequence[SetImplBase]) \
            -> Sequence[SetImplBase]:
        return operands

    def upper_bound_difference_operands(self, operands: Sequence[SetImplBase]) \
            -> Sequence[SetImplBase]:
        return operands

    def upper_bound_custom_set(self, impl: CustomSetImplBase) -> SetImplBase:
        return impl

    def lower_bound_intersection_operands(self, operands: Sequence[SetImplBase]) \
            -> Sequence[SetImplBase]:
        return operands

    def lower_bound_union_operands(self, operands: Sequence[SetImplBase]) \
            -> Sequence[SetImplBase]:
        return operands

    def lower_bound_difference_operands(self, operands: Sequence[SetImplBase]) \
            -> Sequence[SetImplBase]:
        return operands

    def lower_bound_custom_set(self, impl: CustomSetImplBase) -> SetImplBase:
        return impl
