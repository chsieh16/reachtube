from typing import List, Sequence

import numpy as np
from scipy.spatial import Rectangle

from reachtube.rect_set_impl._boxes_map import BoxesMap, _check_boxes_disjoint
from reachtube.set_impl import SetImplBase, EmptySet, BoundingBase


def _find_any_overlapping_box(a: BoxesMap, b: BoxesMap) -> SetImplBase:
    """
    Find a box in the intersection of two boxes maps.
    Can be used to check (non)disjointness

    TODO optimization
    """
    for a_box in a.boxes:
        for b_box in b.boxes:
            if not _check_boxes_disjoint(a_box, b_box):
                new_mins = np.maximum(a_box.mins, b_box.mins)  # type: np.ndarray
                new_maxes = np.minimum(a_box.maxes, b_box.maxes)  # type: np.ndarray
                return BoxesMap.from_extrema(mins=new_mins, maxes=new_maxes)
    return EmptySet()


class UnderApproxBoxes(BoundingBase):
    """
    Simplify a given set by a under-approximation using only EmptySet and BoxesMap.
    Useful for speeding up (non)emptiness query when under-approximation is nonempty.
    """
    def __init__(self) -> None:
        super().__init__(upper=False)

    def lower_bound_intersection_operands(self, operands: Sequence[SetImplBase]) \
            -> Sequence[SetImplBase]:
        """ Under-approximation of intersection by keeping one BoxesMap with a short list of boxes """
        assert len(operands) >= 2
        if len(operands) > 2 or not all(isinstance(op, BoxesMap) for op in operands):
            # TODO better under-approximation for the intersection
            #  of more than two operands
            # TODO better under-approximation for the intersection of other types
            return (EmptySet(),)

        boxes_map = [op for op in operands if isinstance(op, BoxesMap)]  # type: Sequence[BoxesMap]
        return [_find_any_overlapping_box(*boxes_map)]

    def lower_bound_union_operands(self, operands: Sequence[SetImplBase]) \
            -> Sequence[SetImplBase]:
        """ Under-approximation of union by keeping only BoxesMaps and merge them """
        boxes = []  # type: List[Rectangle]
        for op in operands:
            if isinstance(op, BoxesMap):
                boxes.extend(op.boxes)
        return (BoxesMap.from_rectangles(boxes),)

    def lower_bound_difference_operands(self, operands: Sequence[SetImplBase]) \
            -> Sequence[SetImplBase]:
        # TODO better under-approximation for set difference
        return (EmptySet(),)
