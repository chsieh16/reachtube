import abc
from typing import Any, Generic, Sized, TypeVar

from reachtube._abstract_types import NaiveSetMixin, SupportStateStamped
from reachtube._generic_contract import GenericContract
from reachtube._generic_obstacle import GenericObstacle

_StampT = TypeVar('_StampT')
_PointT = TypeVar('_PointT', bound=Sized)
_StateT = TypeVar('_StateT')


class GenericReachtube(Generic[_StampT, _PointT, _StateT],
                       NaiveSetMixin[
                           SupportStateStamped[_StampT, _PointT, _StateT]]):
    """ Reachtube represents the reachable set of states evolving w.r.t
        continuous time. This object of this class should be immutable.

        To interested readers, the binary operators '<=', '&', '|', and '-'
        forms a *generalized Boolean algebra* (or *relatively complemented
        distributive lattice*) where a top element (universal set) and absolute
        complements are not (explicitly) defined. All other operators except
        membership can be defined as below.
    """
    @abc.abstractmethod
    def __repr__(self) -> str:
        """ String representation. Preferably a YAML string """
        raise NotImplementedError

    def __contains__(self, x: SupportStateStamped[_StampT, _PointT, _StateT]) -> bool:
        return self.contains(x)

    def __bool__(self) -> bool:
        return not self.isempty()

    # NOTE Default implementations of relational operators
    # Beware that they are based on only >= and <=
    # Do override with faster implementation or when necessary
    def __eq__(self, other: Any) -> bool:
        if not isinstance(other, GenericReachtube):
            raise NotImplementedError
        return self <= other <= self

    def __ne__(self, other: Any) -> bool:
        return not self == other

    def __le__(self, other: 'GenericReachtube[_StampT, _PointT, _StateT]') -> bool:
        return self.issubset(other)

    def __lt__(self, other: 'GenericReachtube[_StampT, _PointT, _StateT]') -> bool:
        return self <= other and not (self >= other)

    def __ge__(self, other: 'GenericReachtube[_StampT, _PointT, _StateT]') -> bool:
        return self.issuperset(other)

    def __gt__(self, other: 'GenericReachtube[_StampT, _PointT, _StateT]') -> bool:
        return self >= other and not (self <= other)

    # Set operations. Do notice some are over an iterable of other tubes
    def __or__(self, *others: 'GenericReachtube[_StampT, _PointT, _StateT]') \
            -> 'GenericReachtube[_StampT, _PointT, _StateT]':
        return self.union(*others)

    def __and__(self, *others: 'GenericReachtube[_StampT, _PointT, _StateT]') \
            -> 'GenericReachtube[_StampT, _PointT, _StateT]':
        return self.intersection(*others)

    def __sub__(self, *others: 'GenericReachtube[_StampT, _PointT, _StateT]') \
            -> 'GenericReachtube[_StampT, _PointT, _StateT]':
        return self.difference(*others)

    def __xor__(self, other: 'GenericReachtube[_StampT, _PointT, _StateT]') \
            -> 'GenericReachtube[_StampT, _PointT, _StateT]':
        return self.symmetric_difference(other)

    @abc.abstractmethod
    def contains(self, x: SupportStateStamped[_StampT, _PointT, _StateT]) -> bool:
        """ Check if ``x ∈ self``
        Raises
        ------
        NotImplementedError
            If this cannot be exactly determined with current implementation.
        """
        raise NotImplementedError

    @abc.abstractmethod
    def isempty(self) -> bool:
        """ Check if ``self == ∅``
        Raises
        ------
        NotImplementedError
            If this cannot be exactly determined with current implementation.
        """
        raise NotImplementedError

    @abc.abstractmethod
    def isdisjoint(self, other: 'GenericReachtube[_StampT, _PointT, _StateT]') -> bool:
        """ Check if ``self`` is disjoint with ``other``
        Raises
        ------
        NotImplementedError
            If this cannot be exactly determined with current implementation.
        """
        raise NotImplementedError

    @abc.abstractmethod
    def issubset(self, other: 'GenericReachtube[_StampT, _PointT, _StateT]') -> bool:
        """ Check if ``self ⊆ other``
        Raises
        ------
        NotImplementedError
            If this cannot be exactly determined with current implementation.
        """
        raise NotImplementedError

    def issuperset(self, other: 'GenericReachtube[_StampT, _PointT, _StateT]') -> bool:
        """ Check if ``self ⊇ other``
        Raises
        ------
        NotImplementedError
            If this cannot be exactly determined with current implementation.
        """
        return other.issubset(self)

    def union(self, *others: 'GenericReachtube[_StampT, _PointT, _StateT]') \
            -> 'GenericReachtube[_StampT, _PointT, _StateT]':
        """ Return set union
        Raises
        ------
        NotImplementedError
            If this cannot be exactly represented with current implementation.
        """
        raise NotImplementedError

    @abc.abstractmethod
    def intersection(self, *others: 'GenericReachtube[_StampT, _PointT, _StateT]') \
            -> 'GenericReachtube[_StampT, _PointT, _StateT]':
        """ Return set intersection
        Raises
        ------
        NotImplementedError
            If this cannot be exactly represented with current implementation.
        """
        raise NotImplementedError

    def difference(self, *others: 'GenericReachtube[_StampT, _PointT, _StateT]') \
            -> 'GenericReachtube[_StampT, _PointT, _StateT]':
        """ Return set difference
        Raises
        ------
        NotImplementedError
            If this cannot be exactly represented with current implementation.
        """
        raise NotImplementedError

    def symmetric_difference(self, other: 'GenericReachtube[_StampT, _PointT, _StateT]') \
            -> 'GenericReachtube[_StampT, _PointT, _StateT]':
        """ Return set symmetric difference
        Raises
        ------
        NotImplementedError
            If this cannot be exactly represented with current implementation.
        """
        return (self - other) | (other - self)

    @abc.abstractmethod
    def is_colliding(self, obs: GenericObstacle[_PointT]) -> bool:
        """
        Check if reachtube is colliding with given obstacles. More precisely,
        check if there is a stamped state in reachtube whose computed stamped
        position is in the given obstacle. Formally,

            ∃ ts ∈ self, ts.position ∈ obs

        Notes:
            The obstacle can be any closed set of points, e.g., list of boxes.
            It is not necessarily bounded or convex.
        """
        raise NotImplementedError

    @abc.abstractmethod
    def is_violating(self, contr: GenericContract[_StampT, _PointT]) -> bool:
        """
        Check if reachtube is violating a given contract. More precisely,
        check if there is a stamped state in reachtube whose computed stamped
        position is not in the given contract. Formally,

            ∃ ts ∈ self, (ts.stamp, ts.position) ∉ contr
        """
        raise NotImplementedError

    @abc.abstractmethod
    def copy(self) -> 'GenericReachtube[_StampT, _PointT, _StateT]':
        """ Return a shallow copy of ``self`` """
        raise NotImplementedError
