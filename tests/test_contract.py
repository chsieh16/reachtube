import unittest

from scipy.spatial import Rectangle

from reachtube import GenericContract


class ContractTestCase(unittest.TestCase):
    def test_is_disjoint(self):
        a = GenericContract.from_stamped_rectangles(
            [(0.0, Rectangle(mins=[0, 0, 0], maxes=[1, 1, 1])),
             (1.0, Rectangle(mins=[0.5, 0.8, 0.5], maxes=[1.5, 1.5, 1.5]))
             ]
        )
        b = GenericContract.from_stamped_rectangles(
            [(0.0, Rectangle(mins=[1.2, 1.2, 0], maxes=[2, 2, 0.5])),
             (0.5, Rectangle(mins=[1.6, 1.6, 0], maxes=[2.4, 2.8, 0.5])),
             (1.5, Rectangle(mins=[1.6, 1.6, 1.0], maxes=[1.8, 1.8, 1.5]))
             ]
        )
        self.assertTrue(a.isdisjoint(b))

    def test_is_not_disjoint(self):
        a = GenericContract.from_stamped_rectangles(
            [(0.0, Rectangle(mins=[0, 0, 0], maxes=[1, 1, 1])),
             (1.0, Rectangle(mins=[0.5, 0.8, 0.5], maxes=[1.5, 1.5, 1.5]))
             ]
        )
        b = GenericContract.from_stamped_rectangles(
            [(0.0, Rectangle(mins=[0, 0, 0], maxes=[1, 1, 0.5])),
             (0.5, Rectangle(mins=[0, 0.8, 0], maxes=[1, 3, 0.5])),
             (1.5, Rectangle(mins=[0.5, 0.5, 1.0], maxes=[1.5, 1.5, 1.5]))
             ]
        )
        self.assertFalse(a.isdisjoint(b))

    def test_intersection(self):
        a = GenericContract.from_stamped_rectangles(
            [(0.0, Rectangle(mins=[0, 0, 0], maxes=[1, 1, 1])),
             (1.0, Rectangle(mins=[0.5, 0.5, 0.5], maxes=[1.5, 1.5, 1.5]))
             ]
        )
        b = GenericContract.from_stamped_rectangles(
            [(0.0, Rectangle(mins=[0, 0, 0], maxes=[1, 1, 0.5])),
             (0.5, Rectangle(mins=[0, 0.5, 0], maxes=[1, 3, 0.5])),
             (1.5, Rectangle(mins=[0.5, 0.5, 1.0], maxes=[1.5, 1.5, 1.5]))
             ]
        )
        intersect = a & b
        print(intersect)  # TODO Check result instead of printing
        self.assertTrue(bool(intersect))

    def test_is_superset(self):
        a = GenericContract.from_stamped_rectangles(
            [(0.0, Rectangle(mins=[0, 0, 0], maxes=[1, 1, 1])),
             (1.0, Rectangle(mins=[0.5, 0.5, 0.5], maxes=[1.5, 1.5, 1.5]))
             ]
        )
        b = GenericContract.from_stamped_rectangles(
            [(0.0, Rectangle(mins=[0, 0, 0], maxes=[1, 1, 0.5])),
             (0.5, Rectangle(mins=[0, 0.5, 0], maxes=[1, 1, 0.5])),
             (1.0, Rectangle(mins=[0.5, 0.5, 1.0], maxes=[1.5, 1.5, 1.5]))
             ]
        )
        self.assertTrue(a >= b)

    def test_is_not_superset(self):
        a = GenericContract.from_stamped_rectangles(
            [(0.0, Rectangle(mins=[0, 0, 0], maxes=[1, 1, 1])),
             (1.0, Rectangle(mins=[0.5, 0.5, 0.5], maxes=[1.5, 1.5, 1.5]))
             ]
        )
        b = GenericContract.from_stamped_rectangles(
            [(0.0, Rectangle(mins=[0, 0, 0], maxes=[1, 1, 0.5])),
             (0.5, Rectangle(mins=[0, 0.5, 0], maxes=[1, 2, 0.5])),
             (1.0, Rectangle(mins=[0.5, 0.5, 1.0], maxes=[1.5, 1.5, 1.5]))
             ]
        )
        self.assertFalse(b <= a)

    def test_difference(self):
        a = GenericContract.from_stamped_rectangles(
            [(0.0, Rectangle(mins=[0, 0, 0], maxes=[1, 1, 1])),
             (1.0, Rectangle(mins=[0.5, 0.5, 0.5], maxes=[1.5, 1.5, 1.5]))
             ]
        )
        b = GenericContract.from_stamped_rectangles(
            [(0.0, Rectangle(mins=[0, 0, 0], maxes=[1, 1, 0.5])),
             (0.5, Rectangle(mins=[0, 0.5, 0], maxes=[2, 2, 0.5])),
             (1.0, Rectangle(mins=[0.5, 0.5, 1.0], maxes=[1.5, 1.5, 1.5]))
             ]
        )
        b -= a
        print(b)  # TODO Check result instead of printing
        self.assertTrue(bool(b))


if __name__ == '__main__':
    unittest.main()
