from itertools import combinations
from typing import List, Sequence

from scipy.spatial import Rectangle

from reachtube.rect_set_impl._boxes_map import BoxesMap, _check_boxes_disjoint, _check_boxes_subset, _intersect_boxes
from reachtube.set_impl import SimplifierBase, SetImplBase, EmptySet


class SimplifyBoxes(SimplifierBase):
    def simplify_intersection_operands(self, operands: Sequence[SetImplBase]) \
            -> Sequence[SetImplBase]:
        """
        Simplify intersection if any two BoxesMaps are disjoint.
        """
        if sum(isinstance(op, BoxesMap) for op in operands) < 2:
            return operands  # Unable to simplify

        boxes_map_list = [op for op in operands if isinstance(op, BoxesMap)]  # type: List[BoxesMap]
        if any(_check_boxes_map_disjoint(a, b) for a, b in combinations(boxes_map_list, 2)):
            # Exists a pair of boxes that are disjoint
            return (EmptySet(),)

        if len(operands) != 2 or not all(isinstance(op, BoxesMap) for op in operands):
            return operands  # Unable to simplify
        # Special for two BoxesMap objects
        a = operands[0]  # type: BoxesMap
        b = operands[1]  # type: BoxesMap

        new_boxes_list = [
            _intersect_boxes(a_box, b_box)
            for a_box in a.boxes for b_box in b.boxes
            if not _check_boxes_disjoint(a_box, b_box)
        ]  # type: List[Rectangle]

        return [BoxesMap.from_rectangles(new_boxes_list)]

    def simplify_union_operands(self, operands: Sequence[SetImplBase]) \
            -> Sequence[SetImplBase]:
        if sum(isinstance(op, BoxesMap)
               for op in operands) < 2:
            return operands  # Unable to simplify

        boxes = []  # type: List[Rectangle]
        non_boxes = []
        for op in operands:
            if isinstance(op, BoxesMap):
                boxes.extend(op.boxes)
            else:
                non_boxes.append(op)
        assert boxes
        non_boxes.append(BoxesMap.from_rectangles(boxes))
        return non_boxes

    def simplify_difference_operands(self, operands: Sequence[SetImplBase]) \
            -> Sequence[SetImplBase]:
        if sum(isinstance(op, BoxesMap)
               for op in operands) < 2:
            return operands  # Unable to simplify

        head, tail = operands[0], operands[1:]
        non_boxes = self.simplify_union_operands(tail)
        if not isinstance(head, BoxesMap) or len(non_boxes) >= 2:
            return [head] + list(non_boxes)  # Unable to further simplify

        assert len(non_boxes) == 1
        assert isinstance(non_boxes[0], BoxesMap)
        tail_boxes = non_boxes[0].boxes

        # A \ [B1, B2, ...] = ((A \ B1) \ B2 \...)
        remaining_boxes = head.boxes
        for op in tail_boxes:  # type: Rectangle
            # Filter boxes that is a subset of op
            remaining_boxes = [
                box for box in remaining_boxes
                if not _check_boxes_subset(box, op)
            ]  # type: List[Rectangle]

            if len(remaining_boxes) == 0:
                return [EmptySet()]
            # FIXME could there be a bug due to set difference over a closed set becomes a open set?
            if all(_check_boxes_disjoint(op, box)
                   for box in remaining_boxes):
                # op not intersecting any remaining boxes
                continue
            # Calculate the boxes after set difference
            split_remaining_boxes = []
            for box in remaining_boxes:
                if _check_boxes_disjoint(op, box):
                    split_remaining_boxes.append(box)
                    continue
                # else:
                for d in range(len(op.maxes)):  # Split along each dimension
                    if box.mins[d] < op.mins[d] < box.maxes[d]:
                        # Pick the left of the split
                        split_remaining_boxes.append(box.split(d, op.mins[d])[0])
                    if box.mins[d] < op.maxes[d] < box.maxes[d]:
                        # Pick the right of the split
                        split_remaining_boxes.append(box.split(d, op.maxes[d])[1])
            remaining_boxes = split_remaining_boxes

        return [BoxesMap.from_rectangles(remaining_boxes)]


def _check_boxes_map_disjoint(a: BoxesMap, b: BoxesMap) -> bool:
    if all(_check_boxes_disjoint(a.bounding_box, b_box) for b_box in b.boxes) \
            or all(_check_boxes_disjoint(a_box, b.bounding_box) for a_box in a.boxes):
        # Speed up using bounding boxes
        return True
    return all(_check_boxes_disjoint(a_box, b_box)
               for a_box in a.boxes for b_box in b.boxes)
