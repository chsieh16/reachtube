Install/Uninstall
-----------------

At the root folder of the repository,
run the command below to register ``reachtube`` as a Python package.
Note that you don't have to run install again even after you change the code.

.. code-block:: bash

    pip3 install --user -e ./


Un-register ``reachtube`` package with the command

.. code-block:: bash

    pip3 uninstall reachtube


Examples
--------

Take a look in the ``tests`` folder.

.. code-block:: bash

    python3 tests/test_tc_simulate.py
