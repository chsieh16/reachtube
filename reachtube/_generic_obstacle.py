"""
Obstacle represented as a closed set in 3-D space for checking safety.
"""
from typing import Any, Dict, List, Sequence, Sized, TypeVar, Union

import numpy as np
from scipy.spatial import Rectangle

from reachtube._abstract_types import NaiveSetMixin
from reachtube.rect_set_impl import BoxesMap, SimplifyBoxes, UnderApproxBoxes
from reachtube.set_impl import EmptySet, SetImplBase, \
    set_impl_intersection, set_impl_union, \
    EmptinessQuery, MembershipQuery

_PointT = TypeVar('_PointT', bound=Sized)


class GenericObstacle(NaiveSetMixin[_PointT]):
    """ Class exposed to users representing any kind of obstacles. """
    def __init__(self) -> None:
        self._impl = EmptySet()  # type: SetImplBase

    def contains(self, item: _PointT) -> bool:
        return MembershipQuery(item).visit(self._impl)

    def isempty(self) -> bool:
        # FIXME Good way to simplify only once if called multiple times
        self._impl = SimplifyBoxes().visit(self._impl)
        return EmptinessQuery().visit(self._impl)

    def isdisjoint(self, other: 'GenericObstacle[_PointT]') -> bool:
        # Check the intersection is empty
        impl_intersection = set_impl_intersection(self._impl, other._impl)
        # FIXME Good way to simplify only once if called multiple times
        try:
            under_approx = UnderApproxBoxes().visit(impl_intersection)
            if not EmptinessQuery().visit(under_approx):
                return False
        except NotImplementedError:
            pass
        impl = SimplifyBoxes().visit(impl_intersection)
        return EmptinessQuery().visit(impl)

    def issubset(self, other: 'GenericObstacle[_PointT]') -> bool:
        raise NotImplementedError

    def intersection(self, *others: 'GenericObstacle[_PointT]') \
            -> 'GenericObstacle[_PointT]':
        """
        Intersection is represented symbolically.
        Not evaluated unless being queried.
        """
        if not others:
            return self
        impl = set_impl_intersection(self._impl, *[o._impl for o in others])
        return self._from_set_impl(impl)

    # region Explicit constructors
    @classmethod
    def build_obstacle(cls, obs: Union[List[Any], Dict[str, Any], None] = None) \
            -> 'GenericObstacle[_PointT]':
        if not obs:  # None or empty list/dictionary
            return cls()

        if isinstance(obs, dict):
            return cls._from_set_impl(_build_set_impl(obs))
        elif isinstance(obs, list):
            impl_iter = (_build_set_impl(o) for o in obs)
            impl_union = set_impl_union(*list(impl_iter))
            map_impl = SimplifyBoxes().visit(impl_union)
            return cls._from_set_impl(map_impl)
        else:
            raise NotImplementedError("Unsupported obstacle description " + str(obs))

    @classmethod
    def from_rectangles(cls, rect_seq: Sequence[Rectangle]) \
            -> 'GenericObstacle[_PointT]':
        return cls._from_set_impl(BoxesMap.from_rectangles(rect_seq))
    # endregion

    @staticmethod
    def _from_set_impl(impl: SetImplBase) -> 'GenericObstacle[_PointT]':
        """
        Protected constructor hidden from users.
        Construct a obstacle directly from a given set implementation.
        """
        ret = GenericObstacle()  # type: GenericObstacle[_PointT]
        ret._impl = impl
        return ret


def _build_set_impl(obs: Dict[str, Union[List[float], Dict[str, Any]]]) \
        -> SetImplBase:
    """
    Build an obstacle from a dictionary representation modified from
    <collision> defined in Gazebo SDF.
    Several simplifications below:

    + Only <pose> and <geometry> are used
    + <pose> is always w.r.t the world coordinate.

    See http://sdformat.org/spec?ver=1.7&elem=collision
    See the list of shapes: http://sdformat.org/spec?ver=1.7&elem=geometry

    Parameters
    ----------
    obs : Dict[str, Union[List, Dict]]
        Dictionary representation of the obstacle

    Returns
    -------
    GenericObstacle
        Obstacle object built from obs

    Raises
    ------
    NotImplementedError
        TODO Finish documentations
    ValueError
        When obs is not a valid obstacle description
    """
    assert isinstance(obs['pose'], list)
    xyz = np.array(obs['pose'][0:3])
    rpy = np.array(obs['pose'][3:6])
    if any(abs(v) != 0.0 for v in rpy):  # NOTE beware of -0.0
        raise NotImplementedError("Roll, pitch, yaw are not supported yet.")

    geo = obs['geometry']
    if not isinstance(geo, dict):
        raise ValueError("Unsupported geometry description " + str(geo))

    if 'box' in geo:
        scale = np.array(geo['box']['size'][0:3])
        if not np.all(scale > 0):
            raise ValueError("Size for box obstacle should be positive, but"
                             + str(scale.tolist()) + "is given.")
        return BoxesMap.from_extrema(mins=xyz - (scale / 2),
                                     maxes=xyz + (scale / 2))

    raise ValueError("Unsupported Gazebo obstacle description " + str(obs))

