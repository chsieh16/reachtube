import yaml

import numpy as np
import matplotlib.pyplot as plt

from reachtube.rect_set_impl._boxes_map import Map
from reachtube.drone3d_nosymmetry_v1 import tc_simulate, bloat_to_tube


#Function to call both TC simulate and bloatToTube
def getTubes(xf=0, yf=0, zf=0, time=20, initCond=[0,0,0,0,0,0,0,0,0]):
    trace = tc_simulate([xf, yf, zf], initCond, time)
    goal = [xf, yf, zf]
    dimensions = len(trace[0])
    k = [1] * (dimensions - 1)
    gamma = [0] * (dimensions - 1)
    init_delta_array = [0.5,0.5,0.5] + [0.1] * (dimensions - 4)
    tube = bloat_to_tube(init_delta_array, trace, dimensions, goal)
    print("simulate/bloating done")
    return tube

#function to convert a reachtube to a map type
def convertTubeToMap(rtube):
    newTubeType = []
    for j in range(1, len(rtube)-1,2):
        newTubeType.append(( (rtube[j+1][1],rtube[j+1][2],rtube[j+1][3]) , (rtube[j][1],rtube[j][2],rtube[j][3]) ))
    return newTubeType

#function for ploting results
def plotEnv(rtube, collisions):
    for i in range(0, len(rtube)):
        #plot midpoint of reachtube (for visualization in matplotlib)
        rtubePt1X = (rtube[i][0][0] + rtube[i][1][0])/2
        rtubePt1Y = (rtube[i][0][1] + rtube[i][1][1])/2
        check = [[rtube[i][0][0], rtube[i][0][1], rtube[i][0][2]], [rtube[i][1][0], rtube[i][1][1], rtube[i][1][2]]]
        if i in collisions:
            plt.plot(rtubePt1X,rtubePt1Y,'.r')
        else:
            plt.plot(rtubePt1X,rtubePt1Y,'.b')
    plt.show()

#Functino to find findPotentialObstacles
def findPotentialObstacles(rtube):
    rtubePt1X = (rtube[0][0][0] + rtube[0][1][0])/2
    rtubePt1Y = (rtube[0][0][1] + rtube[0][1][1])/2
    rtubePt2X = (rtube[-1][0][0] + rtube[-1][1][0])/2
    rtubePt2Y = (rtube[-1][0][1] + rtube[-1][1][1])/2
    lenReachTube = np.sqrt((rtubePt2X-rtubePt1X)**2 + (rtubePt2Y-rtubePt1Y)**2)
    #print(lenReachTube)
    obsToCheck = []
    for i in range(0, len(obsPts)):
        obsX1 = obsPts[i][0][0]
        obsX2 = obsPts[i][1][0]
        obsY1 = obsPts[i][0][1]
        obsY2 = obsPts[i][1][1]
        obsMidptX = (obsX1 + obsX2)/2
        obsMidptY = (obsY1 + obsY2)/2


        distToObs = np.sqrt((rtubePt1X-obsMidptX)**2 + (rtubePt1Y-obsMidptY)**2)
        distToObs2 = np.sqrt((rtubePt2X-obsMidptX)**2 + (rtubePt2Y-obsMidptY)**2)

        if(lenReachTube >= distToObs or lenReachTube >= distToObs2):
            plt.plot([obsX1, obsX2], [obsY1, obsY2], 'go-')
            obsToCheck.append(walls[i])
        else:
            plt.plot([obsX1, obsX2], [obsY1, obsY2], 'ko-')
    return obsToCheck



if __name__ == "__main__":

    with open('obsPts.data.yaml', 'r') as filehandle:
        obsPts = yaml.safe_load(filehandle)
    with open('walls.data.yaml', 'r') as filehandle:
        walls = yaml.safe_load(filehandle)

    rtube1 = getTubes(xf=15, yf=23, zf=2.5, time=80, initCond=[0,0,0,0,0,0,0,0,0])
    rtube2 = getTubes(xf=23, yf=0, zf=3, time=60, initCond=[15, 23, 2.5,0,0,0,0,0,0])
    rtube3 = getTubes(xf=16, yf=-15, zf=2, time=60, initCond=[23, 0, 3,0,0,0,0,0,0])


    env = Map(walls)
    tube = Map(convertTubeToMap(rtube1))
    obsToCheck = findPotentialObstacles(tube.obstacles)
    print("Found", len(obsToCheck), 'potential obstacles')

    potObs = Map(obsToCheck)

    print(tube.isdisjoint(potObs))
    collisions = tube.find_overlapping_rectangles(potObs)
    plotEnv(tube.obstacles, collisions)

    #For timing differnt potential obstacle method
    # avgTime = 0
    # for x in range(0, 20):
    #     start_time = time.time()
    #     collisions = tube.intersect(env)
    #     elapsed = time.time() - start_time
    #     print("Intersections:", len(collisions), elapsed)
    #     avgTime += elapsed
    # print("Avg Time:", avgTime/20)
