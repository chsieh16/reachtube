import math as m
from typing import List, Sequence, Tuple

from scipy.constants import g as G
from scipy.integrate import odeint
from scipy import linalg

from reachtube.drone3d_types import Reachtube, Drone3dStateStamped
from reachtube._rectangular_generic_reachtube import from_stamped_states as build_tube_from_stamped_states

MASS = 20
ModePara = Tuple[float, ...]
State = Tuple[float, ...]
StateStamped = Tuple[float, ...]
Action = Tuple[float, ...]
DesiredState = Tuple[float, ...]


def bloat_to_tube(k_gamma_delta0: Tuple[Tuple[float, float, float], ...],
                  center_trace: List[StateStamped])\
        -> Reachtube:
    stamped_states = []
    for i in range(len(center_trace)-1):
        time_interval = center_trace[i+1][0] - center_trace[0][0]
        lower_stamp = center_trace[i][0]
        upper_stamp = center_trace[i+1][0]

        lower_state = []
        upper_state = []
        for dim, k_g_d in enumerate(k_gamma_delta0, start=1):
            k, gamma, delta0 = k_g_d
            delta = k * m.exp(gamma*time_interval) * delta0
            lower_state.append(min(center_trace[i+1][dim],
                                   center_trace[i][dim]) - delta)
            upper_state.append(max(center_trace[i+1][dim],
                                   center_trace[i][dim]) + delta)
        stamped_states.append(Drone3dStateStamped(stamp=lower_stamp,
                                                  state=lower_state))
        stamped_states.append(Drone3dStateStamped(stamp=upper_stamp,
                                                  state=upper_state))
    return build_tube_from_stamped_states(stamped_states)


def compute_angle(yaw: float, vec: Tuple[float, float]) -> float:
    """
    Parameters
    ----------
    yaw : float
        Current yaw
    vec : Tuple[float, float]
        TODO
    """
    """
    unit = [1.0, 0.0]^T
    R    = [[cos(yaw), -sin(yaw)],
            [sin(yaw),  cos(yaw)]]
    heading = (R x unit)^T = [cos(yaw), sin(yaw)]
    """
    heading = (m.cos(yaw), m.sin(yaw))
    diff = m.atan2(linalg.det([heading, vec]),
                   heading[0]*vec[0] + heading[1]*vec[1])
    return yaw + diff


def dynamics(state: State, time: float, action: Action)\
        -> State:
    """ Compute derivatives of state to be integrated

    Returns
    -------
    State
        [description]
    """
    # Variables
    (_, _, _, vx, vy, vz, roll, pitch, yaw) = state[:9]
    (fz, w1, w2, w3) = action[:4]

    # Derivatives
    dvx = (m.cos(roll) * m.sin(pitch) * m.cos(yaw) + m.sin(roll) * m.sin(yaw))\
        * fz / MASS
    dvy = (m.cos(roll) * m.sin(pitch) * m.sin(yaw) - m.sin(roll) * m.cos(yaw))\
        * fz / MASS
    dvz = m.cos(roll) * m.cos(pitch) * fz / MASS + G

    droll = w1 + m.sin(roll)*m.tan(pitch)*w2 + m.cos(roll)*m.tan(pitch)*w3
    dpitch = m.cos(roll) * w2 - m.sin(roll) * w3
    dyaw = m.sin(roll) * (1 / m.cos(pitch)) * w2 \
        + m.cos(roll) * (1 / m.cos(pitch)) * w3

    return (vx, vy, vz, dvx, dvy, dvz, droll, dpitch, dyaw)


def control(state: State, desired: DesiredState,
            action: Action) -> Action:
    # Constants
    Kp, Kp_bar, Kd = 0.9, 0.1, 0.1

    # Variables
    (x, y, z, vx, vy, vz, roll, pitch, yaw) = state[:9]
    (x_d, y_d, z_d, vx_d, vy_d, vz_d, roll_d, pitch_d, yaw_d,
     ax_d, ay_d, az_d, droll_d, dpitch_d, dyaw_d) = desired

    # Derivative of state
    (_, _, _, _, _, _, droll, dpitch, dyaw) = dynamics(state[:9], 1.0, action)

    # Feedforward control
    f_ff = -MASS * m.sqrt(ax_d ** 2 + ay_d ** 2 + (az_d - G) ** 2)
    w1_ff = droll_d - m.sin(pitch_d) * dyaw_d
    w2_ff = m.cos(roll_d) * dpitch_d + m.sin(roll_d) * m.cos(pitch_d) * dyaw_d
    w3_ff = -m.sin(roll_d) * dpitch_d + m.cos(roll_d) * m.cos(pitch_d) * dyaw_d

    # Feedback control
    f_fbx = ((m.cos(roll)*m.sin(pitch)*m.cos(yaw) + m.sin(roll)*m.sin(yaw))
             * ((x_d - x)*Kp + (vx_d - vx)*Kd))
    f_fby = ((m.cos(roll)*m.sin(pitch)*m.sin(yaw) - m.sin(roll)*m.cos(yaw))
             * ((y_d - y)*Kp + (vy_d - vy)*Kd))
    f_fbz = m.cos(roll) * m.cos(pitch) * ((z_d - z) * Kp + (vz_d - vz) * Kd)
    f_fb = f_fbx + f_fby + f_fbz

    w1_fb = Kp*(roll_d - roll) + Kd*(droll_d - droll) + Kp_bar*(y_d - y)
    w2_fb = Kp*(pitch_d - pitch) + Kd*(dpitch_d - dpitch) + Kp_bar*(x_d - x)
    w3_fb = Kp*(yaw_d - yaw) + Kd*(dyaw_d - dyaw)

    return (f_ff + f_fb, w1_ff + w1_fb, w2_ff + w2_fb, w3_ff + w3_fb)


def compute_desired_state(state: State, goal: ModePara,
                          time_step: float) -> DesiredState:
    """
    Compute desired state after `time_step` seconds

    Parameters
    ----------
    state : State
        Current state in the following order:
        position, velocity, orientation
    goal : Tup3
        Goal position
    time_step : float

    Returns
    -------
    DesiredState
        Desired state in the following order
        position, velocity, orientation, acceleration, angular velocity
    """
    # State variables
    (x, y, z, vx, vy, vz, roll, pitch, yaw) = state[:9]
    # Compute distance and angle to goal
    v2 = (goal[0] - x, goal[1] - y)
    dist = linalg.norm(v2)
    angle = compute_angle(yaw, v2)
    # Compute desired state

    con = (v2[0]/dist, v2[1]/dist) if dist >= 1 else v2

    x_d = con[0] * time_step + x
    y_d = con[1] * time_step + y
    z_d = (goal[2] - z) * time_step + z
    vx_d = 0.5 * con[0]
    vy_d = 0.5 * con[1]
    vz_d = goal[2] - z
    ax_d = (vx_d - vx)
    ay_d = (vy_d - vy)
    az_d = vz_d - vz

    yaw_d = angle
    beta_a = -ax_d * m.cos(yaw_d) - ay_d * m.sin(yaw_d)
    beta_b = -az_d + G
    beta_c = -ax_d * m.sin(yaw_d) + ay_d * m.cos(yaw_d)
    pitch_d = m.atan2(beta_a, beta_b)
    roll_d = m.atan2(beta_c, m.sqrt(beta_a ** 2 + beta_b ** 2))
    droll_d = -roll
    dpitch_d = -pitch
    dyaw_d = yaw_d - yaw
    return (x_d, y_d, z_d,
            vx_d, vy_d, vz_d,
            roll_d, pitch_d, yaw_d,
            ax_d, ay_d, az_d,
            droll_d, dpitch_d, dyaw_d)


# function to provide traces of the system
def tc_simulate(mode: str, mode_parameters: ModePara,
                time_step: float, num_steps: int,
                init_state: State) -> List[StateStamped]:
    """[summary]

    Parameters
    ----------
    mode : str
        [description]
    mode_parameters : ModePara
        [description]
    time_step : float
        [description]
    num_steps : int
        [description]
    init_state : State
        Initial state in the following order
        position, velocity, orientation
        orientation are the Euler angle defined with the ZYX convention
        and following the order of roll, pitch, yaw

    Returns
    -------
    List[StateStamped]
        [description]

    Raises
    ------
    ValueError
        [description]
    """
    if mode == 'follow_waypoint':
        state_dims = len(init_state)
        N = 16
        # NOTE evenly spaced time points [0, ..., time_step]
        time_seq = [i*time_step/N for i in range(N)] + [time_step]
        # Simulate the system
        state = init_state
        action = (0.0,) * 4
        trace = [(0.0,) + state]  # Initial time start at 0.0
        for i in range(1, num_steps+1):
            desired_state = \
                compute_desired_state(state, mode_parameters, time_step)
            action = control(state, desired_state, action)
            out = odeint(dynamics, state, time_seq, args=(action,))
            state = tuple(out[-1][0:state_dims])
            # Store state in trace
            trace.append((float(i),) + state)
        return trace
    else:
        raise ValueError("Mode: ", mode, "is not defined for the 3D Drone")


def compute_reachtube(mode: str, mode_parameters: Sequence[float],
                      time_step: float, num_steps: int,
                      init_state: Sequence[float]) -> Reachtube:
    if not 0 < num_steps < 65536:
        raise ValueError("Number of steps should be within 1~65535.")
    mode_para = tuple(mode_parameters[:3])
    init_st = tuple(init_state[:9])

    trace = tc_simulate(mode, mode_para,
                        time_step, num_steps,
                        init_st)
    dimensions = len(init_state)
    init_delta_array = [0.5]*3 + [0.1] * (dimensions - 3)
    k = [1.0] * dimensions
    gamma = [0.0] * dimensions
    k_gamma_delta0 = tuple(zip(k, gamma, init_delta_array))
    return bloat_to_tube(k_gamma_delta0, trace)
