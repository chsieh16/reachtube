""" Only explicit constructors are exposed to users """

from typing import Generator, Optional, Sequence, Sized, Tuple, TypeVar

import numpy as np
from scipy.spatial import Rectangle

from reachtube._abstract_types import SupportStateStamped
from reachtube._generic_contract import GenericContract
from reachtube._generic_obstacle import GenericObstacle
from reachtube._generic_reachtube import GenericReachtube

_StampT = TypeVar('_StampT', float, int, covariant=True)  # TODO Allow other time types
_PointT = TypeVar('_PointT', bound=Sized)
_StateT = TypeVar('_StateT')


# region Explicit constructors
def from_stamped_states(stamped_states: Sequence[SupportStateStamped[_StampT, _PointT, _StateT]]) \
        -> GenericReachtube[_StampT, _PointT, _StateT]:
    _check_stamped_states(stamped_states)
    ret = _RectangularGenericReachtube()  # type: _RectangularGenericReachtube[_StampT, _PointT, _StateT]
    ret._rect_arr = _build_rects(stamped_states)
    return ret


def from_rectangles(rect_seq: Sequence[Rectangle]) \
        -> GenericReachtube[_StampT, _PointT, _StateT]:
    _check_rect_seq(rect_seq)
    ret = _RectangularGenericReachtube()  # type: _RectangularGenericReachtube[_StampT, _PointT, _StateT]
    ret._rect_arr = np.array(rect_seq)
    return ret
# endregion


def _check_stamped_states(stamped_states: Sequence[SupportStateStamped[_StampT, _PointT, _StateT]]) \
        -> None:
    if len(stamped_states) % 2 == 1:
        raise ValueError("Odd number of states is in the given list.")
    if len(stamped_states) == 0:
        return  # Empty reachtube

    it = iter(stamped_states)
    prev_t1 = stamped_states[0].stamp
    for ts0, ts1 in zip(it, it):
        curr_t0 = ts0.stamp
        curr_t1 = ts1.stamp
        if prev_t1 > curr_t0:
            raise ValueError("Rectangles are overlapping in time axis.")
        if curr_t0 >= curr_t1:
            raise ValueError("Time points are not strictly increasing.")
        prev_t1 = curr_t1


def _build_rects(stamped_states: Sequence[SupportStateStamped[_StampT, _PointT, _StateT]]) -> np.ndarray:
    arr = np.empty((len(stamped_states)//2,), Rectangle)
    # Reshape so that every two stamped states are grouped to represent a
    # hyper-rectangle
    it = iter(stamped_states)
    for i, (ts0, ts1) in enumerate(zip(it, it)):
        arr[i] = Rectangle(ts0.to_array_like(), ts1.to_array_like())
    return arr


def _check_rect_seq(rect_seq: Sequence[Rectangle]) -> None:
    if len(rect_seq) == 0:
        return  # Empty reachtube
    it = iter(rect_seq)
    prev_rect = next(it)
    for rect in it:
        if prev_rect.maxes[0] > rect.mins[0]:
            raise ValueError("Rectangles are overlapping in time axis.")
        prev_rect = rect


class _RectangularGenericReachtube(GenericReachtube[_StampT, _PointT, _StateT]):
    """ Rectangular reachtube represents the reachtube with a sequence of
        hyper-rectangles over the time-state space.
        The time interval of these rectangles should not overlap, and the time
        interval is interpreted as right-open, i.e., [t_min, t_max),
        so that we can maintain the order between rectangles based on t_min.
    """

    @staticmethod
    def EMPTYSET() -> GenericReachtube[_StampT, _PointT, _StateT]:
        return _RectangularGenericReachtube()

    def __init__(self) -> None:
        """ Default constructor. Returns an empty reachtube."""
        super().__init__()
        self._rect_arr = np.array([])

    def __repr__(self) -> str:
        return str([ts.tolist()
                    for rect in self._rect_arr
                    for ts in (rect.mins, rect.maxes)])

    def contains(self, x: SupportStateStamped[_StampT, _PointT, _StateT]) -> bool:
        raise NotImplementedError

    def isempty(self) -> bool:
        # Check if the list of rectangles is an empty list
        return len(self._rect_arr) == 0

    def isdisjoint(self, other: GenericReachtube[_StampT, _PointT, _StateT]) -> bool:
        if self.isempty() or other.isempty():
            return True
        return (self & other).isempty()

    def issubset(self, other: GenericReachtube[_StampT, _PointT, _StateT]) -> bool:
        if not isinstance(other, _RectangularGenericReachtube):
            raise NotImplementedError("Not needed for now")
        gen_pair = self._gen_time_aligned_pairs_longest(self._rect_arr,
                                                        other._rect_arr)
        for rect, rect_o in gen_pair:
            if not rect_o:
                # There is a time interval in ``self`` not in ``other``
                return False
            if not rect:
                # There is a time interval in ``other`` not in ``self``
                continue  # Skip

            if np.any(rect.mins < rect_o.mins) or \
                    np.any(rect.maxes > rect_o.maxes):
                # A vertex of rect is outside of rect_o
                return False
        return True

    def intersection(self, *others: GenericReachtube[_StampT, _PointT, _StateT]) \
            -> GenericReachtube[_StampT, _PointT, _StateT]:
        if self.isempty() or any(rect.isempty() for rect in others):
            return _RectangularGenericReachtube.EMPTYSET()

        ret = self._rect_arr  # NOTE Be careful not to modify self._rect_arr

        for other in others:
            if not isinstance(other, _RectangularGenericReachtube):
                raise NotImplementedError("Not needed for now")
            temp = []
            gen_pair = self._gen_time_aligned_pairs(ret,
                                                    other._rect_arr)
            for rect, rect_o in gen_pair:
                new_maxes = np.minimum(rect.maxes, rect_o.maxes)
                new_mins = np.maximum(rect.mins, rect_o.mins)

                if np.any(new_mins >= new_maxes):
                    # The rectangles are disjoint
                    continue  # Skip
                # else:
                temp.append(Rectangle(new_maxes, new_mins))
            ret = temp
        return from_rectangles(ret)

    def convex_union(self, *others: GenericReachtube[_StampT, _PointT, _StateT]) \
            -> GenericReachtube[_StampT, _PointT, _StateT]:
        raise NotImplementedError("Not needed for now")

    def difference(self, *others: GenericReachtube[_StampT, _PointT, _StateT]) -> \
            GenericReachtube[_StampT, _PointT, _StateT]:
        raise NotImplementedError("Not needed for now")

    def union(self, *others: GenericReachtube[_StampT, _PointT, _StateT]) \
            -> GenericReachtube[_StampT, _PointT, _StateT]:
        raise NotImplementedError("Not needed for now")

    def copy(self) -> GenericReachtube[_StampT, _PointT, _StateT]:
        ret = _RectangularGenericReachtube() \
            # type: _RectangularGenericReachtube[_StampT, _PointT, _StateT]
        ret._rect_arr = np.copy(self._rect_arr)
        return ret

    def is_colliding(self, obs: GenericObstacle[_PointT]) -> bool:
        # FIXME programmatic way to extract position from state
        #  instead of hard coded dimensions
        self_obs = GenericObstacle.from_rectangles([
            Rectangle(mins=rect.mins[1:4], maxes=rect.maxes[1:4])
            for rect in self._rect_arr])  # type: GenericObstacle[_PointT]
        return not obs.isdisjoint(self_obs)

    def is_violating(self, contr: GenericContract[_StampT, _PointT]) -> bool:
        # TODO
        raise NotImplementedError

    @staticmethod
    def _gen_time_aligned_pairs_longest(a: Sequence[Rectangle],
                                        b: Sequence[Rectangle]) \
            -> Generator[Tuple[Optional[Rectangle], Optional[Rectangle]],
                         None, None]:
        """
        The function generates the pair of rectangles with aligned time horizon
        from the given pair of sequences.

        Yields
        -------
        Tuple[Optional[Rectangle], Optional[Rectangle]]
            A pair of rectangles, or None if the list does not have states in
            that time interval.
        """
        # TODO Generalize to N-sequences
        it_a = iter(a)
        it_b = iter(b)
        next_rect_a = next(it_a, None)
        next_rect_b = next(it_b, None)
        # NOTE the loop should be bounded by the sum of the rectangles because
        # the worst case is where two reachtubes does not share any timepoint
        for _ in range(len(a) + len(b)):
            if not next_rect_a or not next_rect_b:
                # At least one sequence reached the end
                break

            rect_a = next_rect_a
            rect_b = next_rect_b

            a_tmin, a_tmax = rect_a.mins[0], rect_a.maxes[0]
            b_tmin, b_tmax = rect_b.mins[0], rect_b.maxes[0]

            # NOTE at least one iterator advances in each iteration and thus
            #      guarantees the loop bound
            # Non-overlapping cases
            if a_tmax <= b_tmin:
                next_rect_a = next(it_a, None)
                yield (rect_a, None)
                continue
            if b_tmax <= a_tmin:
                next_rect_b = next(it_b, None)
                yield (None, rect_b)
                continue
            # Overlapping cases
            #  Match t_min
            if a_tmin < b_tmin:
                ret_a, rect_a = rect_a.split(0, b_tmin)
                yield (ret_a, None)
            elif a_tmin > b_tmin:
                ret_b, rect_b = rect_b.split(0, a_tmin)
                yield (None, ret_b)
            else:  # a_tmin == b_tmin
                pass  # tmin is already the same. Do nothing

            if a_tmax < b_tmax:
                ret_a, next_rect_a = rect_a, next(it_a, None)
                ret_b, next_rect_b = rect_b.split(0, a_tmax)
            elif a_tmax > b_tmax:
                ret_a, next_rect_a = rect_a.split(0, b_tmax)
                ret_b, next_rect_b = rect_b, next(it_b, None)
            else:  # a_tmax == b_tmax
                # Advance both since time must be strictly increasing
                ret_a, next_rect_a = rect_a, next(it_a, None)
                ret_b, next_rect_b = rect_b, next(it_b, None)
            assert ret_a is not None and ret_b is not None
            yield (ret_a, ret_b)

        if next_rect_a:
            yield (next_rect_a, None)
            yield from ((ret_a, None) for ret_a in it_a)
        elif next_rect_b:
            yield (None, next_rect_b)
            yield from ((None, ret_b) for ret_b in it_b)

    @staticmethod
    def _gen_time_aligned_pairs(a: Sequence[Rectangle],
                                b: Sequence[Rectangle])\
        -> Generator[Tuple[Rectangle, Rectangle],
                     None, None]:

        # FIXME early termination when one list is exhausted.
        yield from (
            (ra, rb) for ra, rb in
            _RectangularGenericReachtube._gen_time_aligned_pairs_longest(a, b)
            if ra is not None and rb is not None
        )


# TODO Convert below to unittests
if __name__ == "__main__":
    rt1 = from_rectangles([
        Rectangle((0, 0, 0), (1, 1, 1)),
        Rectangle((1, 0, 0), (2, 1, 1)),
        Rectangle((2, 0, 0), (3, 1, 1)),
        Rectangle((8, 0, 0), (9, 1, 1)),
        Rectangle((11, 0, 0), (12, 1, 1))
    ])  # type: GenericReachtube[float, Sequence[float], Sequence[float]]

    rt2 = from_rectangles([
        Rectangle((-1, 2, 0), (2.5, 3, 3)),
        Rectangle((2.5, 0.5, 0.5), (3.5, 3, 3)),
        Rectangle((8, 2, 0), (12, 3, 3))
    ])  # type: GenericReachtube[float, Sequence[float], Sequence[float]]

    print(rt1 <= rt2)
    print(str(rt1 & rt2))
    print(rt1.isdisjoint(rt2))
