from typing import NamedTuple, Sequence

from reachtube._generic_contract import GenericContract
from reachtube._generic_obstacle import GenericObstacle
from reachtube._generic_reachtube import GenericReachtube

# region Instantiate generic types with concrete types
RealPoint = NamedTuple(
    'RealPoint',
    [('x', float), ('y', float), ('z', float)],
)
RealStamp = float
Drone3dState = Sequence[float]

Obstacle = GenericObstacle[RealPoint]
Contract = GenericContract[RealStamp, RealPoint]
Reachtube = GenericReachtube[RealStamp, RealPoint, Drone3dState]
# endregion


# region Classes for SupportPointStamped and SupportStateStamped protocols
class RealPointStamped:
    def __init__(self, stamp: RealStamp, position: RealPoint) -> None:
        self._time_point = (stamp,) + tuple(position)

    @property
    def stamp(self) -> RealStamp:
        return self._time_point[0]

    @property
    def position(self) -> RealPoint:
        return RealPoint(*self._time_point[1:4])

    def to_array_like(self) -> Sequence[float]:
        return self._time_point


class Drone3dStateStamped:
    def __init__(self, state: Drone3dState, stamp: RealStamp) -> None:
        super().__init__()
        self._time_state = (stamp,) + tuple(state)

    @property
    def stamp(self) -> RealStamp:
        return self._time_state[0]

    @property
    def position(self) -> RealPoint:
        return RealPoint(*self._time_state[1:4])

    @property
    def state(self) -> Drone3dState:
        return self._time_state[1:]

    def to_array_like(self) -> Sequence[float]:
        return self._time_state
# endregion
