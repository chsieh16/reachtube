import unittest

from scipy.spatial import Rectangle

from reachtube.contiguous_union_set_impl import ContiguousUnion, SimplifyContiguousUnion
from reachtube.rect_set_impl import BoxesMap
from reachtube.set_impl import EmptySet, set_impl_difference, EmptinessQuery


class MyTestCase(unittest.TestCase):
    def test_something(self):
        self.assertEqual(True, False)

    def test_is_subset(self):
        t_seq = [
            300.581,
            305.581,
            310.581,
            315.581,
            320.581,
        ]
        rect_1 = Rectangle(mins=[5.8, -0.2, 0.8], maxes=[6.2, 0.2, 1.2])
        rect_2 = Rectangle(mins=[-0.2, -0.2, -0.2], maxes=[0.2, 0.2, 1.2])
        rect_3 = Rectangle(mins=[-0.2, -0.2, 0.8], maxes=[2.2, 0.2, 1.2])
        rect_4 = Rectangle(mins=[1.8, -0.2, 0.8], maxes=[4.2, 0.2, 1.2])
        rect_5 = Rectangle(mins=[5.8, -0.2, 0.8], maxes=[6.2, 0.2, 1.2])
        set_impl_seq_a = [
            BoxesMap.from_rectangles([rect_1, rect_2]),
            BoxesMap.from_rectangles([rect_1, rect_3]),
            BoxesMap.from_rectangles([rect_1, rect_4]),
            EmptySet(),
            BoxesMap.from_rectangles([rect_1, rect_5]),
        ]
        set_impl_seq_b = [
            BoxesMap.from_rectangles([rect_1, rect_2]),
            BoxesMap.from_rectangles([rect_1, rect_3]),
            BoxesMap.from_rectangles([rect_1, rect_4]),
            EmptySet(),
            BoxesMap.from_rectangles([rect_1, rect_5]),
        ]
        a = ContiguousUnion(t_seq, set_impl_seq_a)
        b = ContiguousUnion(t_seq, set_impl_seq_b)

        diff_impl = set_impl_difference(a, b)
        diff_impl = SimplifyContiguousUnion().visit(diff_impl)
        self.assertTrue(EmptinessQuery().visit(diff_impl))


if __name__ == '__main__':
    unittest.main()
