from typing import List, Optional, Sequence, Sized, Tuple, TypeVar, Iterator

import numpy as np
from scipy.spatial import Rectangle

from reachtube.set_impl import CustomSetImplBase, SetImplBase, EmptySet

_ElemT = TypeVar('_ElemT', bound=Sized)


class Map:
    """
        Map represents a group of static obstacles that are present in a given environment
    """

    def __init__(self, elements: List[Tuple[Tuple[float, float, float], Tuple[float, float, float]]]) -> None:
        self.obstacles = elements

    def contains(self, x: Tuple[Tuple[float, float, float], Tuple[float, float, float]]) -> bool:
        if x in self.obstacles:
            return True
        else:
            return False

    def isempty(self) -> bool:
        return len(self.obstacles) == 0

    def find_overlapping_rectangles(self, obstacles: 'Map') -> List[int]:
        """
            Determine all overlapping rectangles between 2 Map objects
        """
        overlap_rect_idx = []
        for i in range(0, len(obstacles.obstacles)):
            lower = obstacles.obstacles[i][0]
            upper = obstacles.obstacles[i][1]
            rect1 = np.array([[lower[0], lower[1], lower[2]], [upper[0], upper[1], upper[2]]])
            for j in range(0, len(self.obstacles)):
                rect2 = np.array([[self.obstacles[j][0][0], self.obstacles[j][0][1], self.obstacles[j][0][2]],
                                  [self.obstacles[j][1][0], self.obstacles[j][1][1], self.obstacles[j][1][2]]])
                val = self._do_rects_inter(rect1, rect2)
                if val:
                    overlap_rect_idx.append(j)
        return overlap_rect_idx

    def isdisjoint(self, obstacles: 'Map') -> bool:
        """Determine if two Map objects are disjoint"""

        overlap_rect_idx = self.find_overlapping_rectangles(obstacles)
        return len(overlap_rect_idx) == 0

    def _do_rects_inter(self, rect1: np.array, rect2: np.array) -> bool:
        if self._check_rect_empty(rect1) or self._check_rect_empty(rect2):
            return False
            # raise ValueError("Do no pass empty rectangles to intersect function", rect1, rect2)
        for i in range(rect1.shape[1]):
            if rect1[0, i] > rect2[1, i] or rect1[1, i] < rect2[0, i]:
                return False
        return True

    def _check_rect_empty(self, rect: np.array) -> bool:
        return not np.all(rect[0, :] <= rect[1, :])


def _in_box(item: _ElemT, rect: Rectangle) -> bool:
    item_arr = np.array(item)
    return all(rect.mins <= item_arr) and all(item_arr <= rect.maxes)


class BoxesMap(CustomSetImplBase):
    def __init__(self, rect_seq: Sequence[Rectangle]):
        assert len(rect_seq) >= 1
        # TODO Optimization.
        self._boxes = tuple(rect_seq)
        self._bounding_box = None  # type: Optional[Rectangle]

    def contains(self, x: _ElemT) -> bool:
        return any(_in_box(x, box) for box in self._boxes)

    def isempty(self) -> bool:
        return not self._boxes

    @property
    def boxes(self) -> Sequence[Rectangle]:
        return self._boxes

    @property
    def bounding_box(self) -> Rectangle:
        if not self._bounding_box:
            mins_arr = np.array([box.mins for box in self._boxes])
            maxes_arr = np.array([box.maxes for box in self._boxes])
            new_mins = np.amin(mins_arr, axis=0)
            new_maxes = np.amax(maxes_arr, axis=0)
            self._bounding_box = Rectangle(mins=new_mins, maxes=new_maxes)
        return self._bounding_box

    @classmethod
    def from_extrema(cls, maxes: _ElemT, mins: _ElemT) \
            -> 'BoxesMap':
        if len(maxes) != len(mins):
            raise ValueError
        if np.any(np.array(mins) > np.array(maxes)):
            raise ValueError("Some min is greater than its corresponding max")
        return cls((Rectangle(maxes=maxes, mins=mins),))

    @classmethod
    def from_rectangles(cls, rect_seq: Sequence[Rectangle]) -> SetImplBase:
        if len(rect_seq) >= 1:
            return cls(rect_seq)
        else:
            return EmptySet()


def _check_boxes_disjoint(a: Rectangle, b: Rectangle) -> bool:
    new_mins = np.maximum(a.mins, b.mins)  # type: np.ndarray
    new_maxes = np.minimum(a.maxes, b.maxes)  # type: np.ndarray
    return bool(np.any(new_mins > new_maxes))


def _check_boxes_subset(a: Rectangle, b: Rectangle) -> bool:
    return np.all(b.mins <= a.mins) and np.all(a.maxes <= b.maxes)


def _intersect_boxes(a: Rectangle, b: Rectangle) -> Rectangle:
    assert not _check_boxes_disjoint(a, b)
    new_mins = np.maximum(a.mins, b.mins)  # type: np.ndarray
    new_maxes = np.minimum(a.maxes, b.maxes)  # type: np.ndarray
    return Rectangle(mins=new_mins, maxes=new_maxes)
