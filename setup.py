#!/usr/bin/env python3

from setuptools import setup

setup(
    name='reachtube',
    version='0.1',
    description='Reachtube computation',
    author='Chuchu Fan, Hussein Sibai',
    maintainer='Chiao Hsieh',
    maintainer_email='chsieh16@illinois.edu',
    license='NCSA',
    packages=setuptools.find_packages(),
    python_requires='>=3.5.2',
    install_requires=[
        'numpy',
        'PyYaml',
        'scipy',
        'typing-extensions',
    ],
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Science/Research',
        'Topic :: Scientific/Engineering',
        'License :: OSI Approved :: University of Illinois/NCSA Open Source License',
        'Programming Language :: Python :: 3.5',
    ]
)
