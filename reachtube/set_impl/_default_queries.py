import abc
from typing import TypeVar

from reachtube.set_impl._visitor_base import SetImplVisitorBase, \
    EmptySet, SetIntersection, SetUnion, SetDifference, CustomSetImplBase


class EmptinessQuery(SetImplVisitorBase[bool], abc.ABC):
    def visit_empty_set(self, tree: EmptySet) -> bool:
        return True

    def visit_intersection(self, tree: SetIntersection) -> bool:
        """
        Check emptiness using the following rules. S is any nonempty set.

            * ⋂ [..., ∅, ...] == ∅
            * ⋂ [S, S, S] != ∅

        Notes:
            Please avoid overriding this function.
            If disjointness between nonempty operands can be proven, do write
            a simplifier to simplify set intersections to empty sets.
        """
        assert len(tree.operands) >= 2
        if any(self.visit(op) for op in tree.operands):
            return True
        if all(tree.operands[0] == op for op in tree.operands):
            return False
        # else:
        raise NotImplementedError("Emptiness query for set intersection is not"
                                  " fully solvable via syntactic rules."
                                  " Please try apply more simplifications.")

    def visit_union(self, tree: SetUnion) -> bool:
        return all(self.visit(op) for op in tree.operands)

    def visit_difference(self, tree: SetDifference) -> bool:
        """
        Check emptiness using the following rules. S is any nonempty set.

            * (S / [S, ...]) == ∅
            * (∅ / [...]) == ∅
            * (S / ∅) != ∅

        Notes:
            Please avoid overriding this function.
            If head ⊆ (⋃ tail) can be proven, write a simplifier to simplify
            set differences to empty sets.
        """
        assert len(tree.operands) >= 2
        head, tail = tree.operands[0], tree.operands[1:]
        if any(head == op for op in tail):
            return True
        if self.visit(head):
            return True
        elif all(self.visit(op) for op in tail):
            return False
        # else:
        raise NotImplementedError("Emptiness query for set difference is not"
                                  " fully solvable via syntactic rules."
                                  " Please try apply more simplifications.")

    def visit_custom_set(self, tree: CustomSetImplBase) -> bool:
        return tree.isempty()


_ElemT = TypeVar('_ElemT')


class MembershipQuery(SetImplVisitorBase[bool], abc.ABC):
    def __init__(self, elem: _ElemT) -> None:
        self._elem = elem

    def visit_empty_set(self, tree: EmptySet) -> bool:
        return False

    def visit_intersection(self, tree: SetIntersection) -> bool:
        # All operands should contain the element
        return all(self.visit(op) for op in tree.operands)

    def visit_union(self, tree: SetUnion) -> bool:
        # Any operands contains the element
        return any(self.visit(op) for op in tree.operands)

    def visit_difference(self, tree: SetDifference) -> bool:
        assert len(tree.operands) >= 2
        op_iter = iter(tree.operands)
        head = next(op_iter)
        return self.visit(head) and not any(self.visit(op) for op in op_iter)

    def visit_custom_set(self, tree: CustomSetImplBase) -> bool:
        return tree.contains(self._elem)
