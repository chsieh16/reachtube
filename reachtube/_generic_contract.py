from typing import Generic, Sequence, Tuple, TypeVar

from scipy.spatial import Rectangle
import yaml

from reachtube._abstract_types import NaiveSetMixin, SupportPointStamped
from reachtube._default_repr import ToYamlType
from reachtube.set_impl import EmptySet, SetImplBase, \
    set_impl_intersection, set_impl_union, set_impl_difference, \
    EmptySetSimplifier, Flatten, \
    EmptinessQuery, MembershipQuery
from reachtube.rect_set_impl import BoxesMap
from reachtube.contiguous_union_set_impl import ContiguousUnion, SimplifyContiguousUnion

_StampT = TypeVar('_StampT')
_PointT = TypeVar('_PointT')


class GenericContract(Generic[_StampT, _PointT],
                      NaiveSetMixin[SupportPointStamped[_StampT, _PointT]]):
    """
    GenericContract is representing a set of stamped points.

    Notes:

        * Stamped points can be any class with matching generic type parameters
          as well as properties defined in SupportPointStamped protocol.
    """

    def __init__(self) -> None:
        self._impl = EmptySet()  # type: SetImplBase

    def __repr__(self) -> str:
        return yaml.safe_dump(ToYamlType().visit(self._impl), default_flow_style=True)

    def __or__(self, other: 'GenericContract[_StampT, _PointT]') \
            -> 'GenericContract[_StampT, _PointT]':
        return self.union(other)

    def __sub__(self, other: 'GenericContract[_StampT, _PointT]') \
            -> 'GenericContract[_StampT, _PointT]':
        return self.difference(other)

    def contains(self, x: SupportPointStamped[_StampT, _PointT]) -> bool:
        return MembershipQuery(x).visit(self._impl)

    def isempty(self) -> bool:
        return EmptinessQuery().visit(self._impl)

    def isdisjoint(self, other: 'GenericContract[_StampT, _PointT]') -> bool:
        impl_intersection = set_impl_intersection(self._impl, other._impl)
        impl = self._simplifier(impl_intersection)
        return EmptinessQuery().visit(impl)

    def issubset(self, other: 'GenericContract[_StampT, _PointT]') -> bool:
        diff_impl = set_impl_difference(self._impl, other._impl)
        impl = self._simplifier(diff_impl)
        return EmptinessQuery().visit(impl)

    def intersection(self, *others: 'GenericContract[_StampT, _PointT]') \
            -> 'GenericContract[_StampT, _PointT]':
        intersect_impl = set_impl_intersection(self._impl, *[o._impl for o in others])
        impl = self._simplifier(intersect_impl)
        return self._from_set_impl(impl)

    def union(self, *others: 'GenericContract[_StampT, _PointT]') \
            -> 'GenericContract[_StampT, _PointT]':
        union_impl = set_impl_union(self._impl, *[o._impl for o in others])
        impl = self._simplifier(union_impl)
        return self._from_set_impl(impl)

    def difference(self, *others: 'GenericContract[_StampT, _PointT]') \
            -> 'GenericContract[_StampT, _PointT]':
        diff_impl = set_impl_difference(self._impl, *[o._impl for o in others])
        impl = self._simplifier(diff_impl)
        return self._from_set_impl(impl)

    @staticmethod
    def _simplifier(impl: SetImplBase) -> SetImplBase:
        # TODO Command pattern
        impl = Flatten().visit(impl)
        impl = EmptySetSimplifier().visit(impl)
        return SimplifyContiguousUnion().visit(impl)

    @staticmethod
    def _from_set_impl(impl: SetImplBase) -> 'GenericContract[_StampT, _PointT]':
        """
        Protected constructor hidden from users.
        Construct a obstacle directly from a given set implementation.
        """
        ret = GenericContract()  # type: GenericContract[_StampT, _PointT]
        ret._impl = impl
        return ret

    @classmethod
    def from_stamped_rectangles(cls, stamped_rect_seq: Sequence[Tuple[_StampT, Rectangle]]) -> \
            'GenericContract[_StampT, _PointT]':
        t_seq, rect_seq = zip(*stamped_rect_seq)
        boxes_seq = tuple(BoxesMap.from_rectangles((rect,)) for rect in rect_seq)
        ret = cls()
        ret._impl = ContiguousUnion(t_seq, boxes_seq)
        return ret

    def num_rects(self) -> int:
        if isinstance(self._impl, EmptySet):
            return 0
        if isinstance(self._impl, ContiguousUnion):
            if not all(isinstance(region, EmptySet) or isinstance(region, BoxesMap)
                       for _, region in self._impl):
                raise NotImplementedError("Cannot count non boxes")
            return sum(len(region.boxes) for _, region in self._impl if isinstance(region, BoxesMap))
        else:
            raise NotImplementedError("Cannot count non ContiguousUnion")
